class JsonStorage {
	constructor() {
		this.storage = localStorage;
	}
	
	getItem(key) {
		return this.unSerialize(this.storage.getItem(key));
	}
	
	
	setItem(key, value) {
		this.storage.setItem(key, this.serialize(value));
		return value;
	}
	
	serialize(value) {
		return JSON.stringify(value);
	}
	
	unSerialize(value) {
		return JSON.parse(value);
		//return jQuery.parseJSON(value);
	}
	
	removeItem(key) {
		this.storage.removeItem(key);
	}
	
	clear() {
		this.storage.clear();
	}
};