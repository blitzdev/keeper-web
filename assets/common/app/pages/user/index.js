const AppPagesUserIndex = {
	template: '#app-pages-user-index',
	props: [ 'pageName', 'user', ],
	data: function () {
        var vm = this,
            funcTimer = null;

        
        funcTimer = function () {
            window.setTimeout(vm.update, 15 * 1000);
            vm.update();
        };
        
        funcTimer();

		return {
            walletList: [],
		};
	},
	methods: {
	    update: function () {
	        let vm = this;
	        
    	    user.api.apiWalletGetListAjax ({}, function (data) {
            	if (data.list.length == 0) {
                    router.push({
                    	name: 'app-pages-user-add-wallet',
                    });
    			} else {
                    vm.walletList = data.list;
    			}
            }, function () {});
	    }
    },
};