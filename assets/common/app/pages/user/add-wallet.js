const AppPagesUserAddWallet = {
	template: '#app-pages-user-add-wallet',
	data: function () {
		let vm = this;


		return {
			'wallet': '',
			'system': '',
			'comment': '',
			'api_id': '',
			'api_key': '',
			sendDisabled: false,
			systemList: getSystemList(),
		};
	},
	methods: {
		submitAdd: function () {
			let vm = this;

            if (vm.sendDisabled) {
                return;
            }

			vm['api_id'] = vm['system'] === 'qiwi' ? vm['wallet'] : vm['api_id'];
            vm.sendDisabled = true;
            user.api.apiWalletAddAjax ({
                "wallet_id": vm['wallet'],
				"wallet": vm['wallet'],
                "system": vm['system'],
                "comment": vm['comment'],
                "api_id": vm['api_id'],
                "api_key": vm['api_key'],
            }, function (data) {
                vm.sendDisabled = false;
                Flash.ok(data.message);
                router.push({
                    name: 'app-pages-user-index',
                });
                vm.clear();
            }, function () {
                vm.sendDisabled = false;
            });


            return false;
		},
		clear() {
			let vm = this;

			vm['wallet'] = '';
            vm['system'] = '';
            vm['comment'] = '';
            vm['api_id'] = '';
            vm['api_key'] = '';
		}
	},
    /*watch: {
        '$route': function (to, from) {
        }
    }*/
};