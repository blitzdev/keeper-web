const AppPagesUserRemoveWallet = {
	template: '<div>Удаление кошелька....</div>',
	data: function () {
		let vm = this;



		return {
		};
	},
	methods: {
		remove: function () {
			let vm = this;

            user.api.apiWalletDeleteAjax ({
                "wallet_id": vm.$route.params.id,
            }, function (data) {
                Flash.ok(data.message);
            }, function () {});
		}
	},
	
	mounted: function () {
	    let vm = this,
	        res = confirm("Вы действительно хотите удалить?");
	    if (res) {
	        vm.remove();
	    }
        router.push({
            name: 'app-pages-user-index',
        });
	}
    /*watch: {
        '$route': function (to, from) {
            this.remove();
            router.push({
                name: 'app-pages-user-index',
            });
        }
    }*/

	/*beforeRouteUpdate: function (to, from, next) {
	 	to.remove();
	}*/
};