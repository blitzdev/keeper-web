const AppPagesUserWallet = {
	template: '#app-pages-user-wallet',
	data: function () {
        let vm = this,
            user = null,
            funTimer = null;

        user = vm.$root.user;
        vm.walletId = vm.$route.params.id;
        vm.wallet = vm.$route.params.number;
        vm.system = vm.$route.params.system;

		vm.$root.pageName = "Страница кошелька";

		vm.render();
		
        
        //funTimer();


		return {
			walletId: vm.$route.params.id,
            wallet: vm.$route.params.number,
            system: vm.$route.params.system,
			user: user,
			balanceList: [],
            historyList: [],

            sendDisabled: false,

            amount: '',
            currency: '',
            comment: '',
            walletTo: '',
		};
	},
	methods: {
		render: function () {
			let vm = this,
                user = null;
                
            vm.clear();

			user = vm.$root.user;
            vm.walletId = vm.$route.params.id;
            vm.wallet = vm.$route.params.number;
            vm.system = vm.$route.params.system;

            user.api.apiGetBalancesAjax ({
                "wallet_id": vm.walletId,
            }, function (data) {
                if (JSON.stringify(vm.balanceList) !== '[]' && data && data.list && JSON.stringify(data.list) !== '[]' && JSON.stringify(vm.list) !== JSON.stringify(data.list)) {
                    //vm.balanceChangeColor = true;
                    
                    var str = "";
                    
                    for (var currency in vm.balanceList) {
                        if (vm.balanceList[currency] != data.list[currency]) {
                            str += formatCurrency(data.list[currency] - vm.balanceList[currency], currency) + " " + currency.toUpperCase();
                            break;
                        }
                    }
                    
                    alertPlay();
                    sendNotify("Баланс на вашем кошельке " + vm.system + "(" + vm.wallet + ")" + " изменился на " + str + "!", "Keeper - все кошельки с тобой!");
                }
                vm.balanceList = data.list;
            }, function () {});


            user.api.apiGetHistoryListAjax ({
                "wallet_id": vm.walletId,
            }, function (data) {
                vm.historyList = data.list;
            }, function () {});
		},

        submitTransfer: function () {
		    let vm = this;
		    
		    vm.sendDisabled = true;
		    
            user.api.apiSendMoneyAjax ({
                "wallet_id": vm.walletId,
                "sum": vm.amount,
                "currency": vm.currency,
                "wallet_to": vm.walletTo,
                "comment": vm.comment,
            }, function (data) {
                vm.sendDisabled = false;
                Flash.ok(data.message);

                vm.amount = '';
                vm.currency = '';
                vm.walletTo = '';
                vm.comment = '';
            }, function () {
                vm.sendDisabled = false;
            });
        },
        
        clear: function () {
            let vm = this;
            
            vm.balanceList = [];
            vm.historyList = [];
        }
	},


    /*beforeRouteUpdate: function (to, from, next) {
        to.render();
	    next();
    }*/

    watch: {
        '$route': function (to, from) {
            this.render();
        }
    }
};