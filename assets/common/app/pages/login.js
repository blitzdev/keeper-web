const AppPagesLogin = {
	template: '#app-pages-login',
	data: function () {
        this.$root.pageName = "Авторизация";


		return {
			licenseKey: "",
			password: "",
		};
	},
	methods: {
		formLoginSubmit: function () {
			let res = user.signin(this.licenseKey, this.password);
			if (res.error) {
				Flash.err(res.message);
				return;
			}
			router.push({
				name: 'app-pages-user-index',
			});
		},
        formGetFreeLicenseSubmit: function () {
			let vm = this, keeper = new KeeperAPI("test", "test");

            keeper.apiGetFreeLicenseAjax({}, function (data) {
                vm.licenseKey = data['license_key'];
                Flash.ok('Лицензия успешно получена.');
                Flash.ok('Лицензия действительна до: ' + data['date_expiration']);
                Flash.ok('Нажми Войти для входа.');
            }, function () {});
			return false;
        }
	},
};