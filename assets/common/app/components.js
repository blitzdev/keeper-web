/*Vue.component('format-system', {
	props: ['system',],
	template: '<img :src="url" width="30px" height="30px">',
	data: function () {
		let url = "assets/common/img/ps/" + this.system.toLowerCase() + ".png";
		return {
			url: url,
		}
	}
});*/



Vue.component('format-system', {
	props: ['system',],
	template: '<div :class="this.data" style="background-size: 70px;background-repeat: no-repeat;"></div>',
	data: function () {
		let data = {};
		data['img-' + this.system.toLowerCase()] = true;
		return {
			data: data,
		}
	}
});


Vue.component('format-status', {
	props: ['status',],
	template: '<div class="status_ok" v-if="status == 1"></div>'
		+ '<div class="status_wrong" v-else></div>',
	/*'<span class="glyphicon glyphicon-ok" aria-hidden="true"  style="color: green;" v-if="status == 1"></span>'
	 + '<span class="glyphicon glyphicon-time" aria-hidden="true" style="color: orange;" v-else></span>',*/
	 
});


Vue.component('format-currency', {
	props: ['currency',],
	template: //"<i>{{ currency }}</i>",//
	'<span class="glyphicon" :class="this.data" aria-hidden="true" style="font-size: 1.333rem;"></span>',
	data: function () {
		let data = {};
		data['glyphicon-' + this.currency.toLowerCase()] = true;
		return {
			data: data,
		}
	}
});






Vue.component('app-header-menu', {
	props: ['user', 'showMenu', 'projectName', 'pageName', 'amount', 'currency', ],
	template: '#app-header-menu',
	methods: {
        menuToggle: function () {
            let $this = $('.nav-toggle');
            //this.showMenu = !this.showMenu;
            console.log(1);

            if($this.hasClass('open')) {
                $('.nav').addClass('nt');
                $this.addClass('toggle close');
                $this.removeClass('open');
                $('.overlord').css('display','block');
            } else {
                $('.overlord').css('display','none');
                $('.nav').removeClass('nt');
                function slide(){$('.nav > .nav-toggle').removeClass('toggle')};
                setTimeout(slide(),500);
                $this.addClass('open');
            }
        },
	}
});




Vue.component('app-right-side', {
    props: [  ],
    template: '#app-right-side',
    data: function () {
        this.render();
        return {
            walletList: [],
        };
    },
    methods: {
        render: function () {
            var vm = this;
            user.api.apiWalletGetListAjax ({}, function (data) {
                vm.walletList = data.list;
            }, function () {});
        },
    },
    watch: {
        '$route': function (to, from) {
            this.render();
        },
    }
});


Vue.component('app-purse-card-info', {
    props: [ "walletId", "system", "wallet", ],
    template: '#app-purse-card-info',
    data: function () {
        return {
            list: [],
            interval: null,
            balanceChangeColor: false,
            //oldBalance: false,
        };
    },
    
    methods: {
        updateBalance: function () {
            let vm = this;
            user.api.apiGetBalancesAjax ({
                "wallet_id": vm.walletId,
            }, function (data) {
                vm.balanceChangeColor = false;
                if (JSON.stringify(vm.list) !== '[]' && data && data.list && JSON.stringify(data.list) !== '[]' && JSON.stringify(vm.list) !== JSON.stringify(data.list)) {
                    vm.balanceChangeColor = true;
                    
                    var str = "";
                    
                    for (var currency in vm.list) {
                        if (vm.list[currency] != data.list[currency]) {
                            str += formatCurrency(data.list[currency] - vm.list[currency], currency) + " " + currency.toUpperCase();
                            break;
                        }
                    }
                    
                    sendNotify("Баланс на вашем кошельке " + vm.system + "(" + vm.wallet + ")" + " изменился на " + str, "Keeper - все кошельки с тобой!");
                    alertPlay();
                }
                vm.list = data.list;
            }, function () {});
        }
    },
    
    mounted: function () {
        let vm = this;        

        vm.updateBalance();
        vm.interval = window.setInterval(vm.updateBalance, 12 * 1000);
    },

    beforeDestroy: function() {
        let vm = this;
        window.clearInterval(vm.interval);
    }
});



