const routes = [
    
	{
		path: '/',
		redirect: {
			name: 'app-pages-login',
		},
		name: 'app-pages-index',
	},
	{
		path: '/login/',
		alias: '/',
		component: AppPagesLogin,
		name: 'app-pages-login',
	},
	{
		path: '/logout/',
		name: 'app-pages-exit',
	},
	{
		path: '/user/index/',
		component: AppPagesUserIndex,
		name: 'app-pages-user-index',
	},
    {
        path: '/user/add-wallet/',
        component: AppPagesUserAddWallet,
        name: 'app-pages-user-add-wallet',
    },
	{
		path: '/user/wallet/:id/:number/:system',
		component: AppPagesUserWallet,
		name: 'app-pages-user-wallet',
	},

    {
        path: '/user/remove-wallet/:id/',
        component: AppPagesUserRemoveWallet,
        name: 'app-pages-user-remove-wallet',
    },
];


const router = new VueRouter({
	routes,
});