function formatCurrency(sum, currency) {
	sum = +sum;
	currency = currency || user.getCurrency();
	if (currency.toUpperCase() == "BTC") {
		return sum.toFixed(8);
	}
	return sum.toFixed(2);
}


function formatDate(date) {
	date = 1000 * date;
	return moment(date).format('L') + ' ' + moment(date).format('LT');
}



function formatDateHours(date) {
	date = 1000 * date;
	return moment(date).format('LT');
}

function formatDateMonth(date) {
	date = 1000 * date;
	return moment(date).format('L');
}

function objectMerge(a, b) {
	return Object.assign(a, b);
}

function getSystemList() {
	return [
		'AdvCash',
		'Payeer',
		'PerfectMoney',
		'Qiwi',
	];
}

function formatSystem(system) {
	switch(system.toLowerCase()) {
        case 'advcash': return 'AdvCash';
        case 'payeer': return 'Payeer';
        case 'perfectmoney': return 'PerfectMoney';
        case 'yandexmoney': return 'YandexMoney';
        case 'qiwi': return 'Qiwi';
		default:return '';
	}
}


//проиграть уведомление
function alertPlay() {
    document.getElementById("audio-block").play();
}


//десктопное уведомление
function sendNotify(text, title='') {
    if (Notification.permission !== "granted") {
        Notification.requestPermission();
    } else {
        var notification = new Notification(title, {
          icon: 'https://keeper.blitz-market.ru/assets/common/images/logo-notification.png',
          body: text,
        });
    
        /*notification.onclick = function () {
          window.open("http://stackoverflow.com/a/13328397/1269037");      
        };*/
    }
 }