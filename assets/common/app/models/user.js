class User {
	constructor() {
		this.userIsAuth = false;
		this.data = {};
		
		this.licenseKeyList = {};
		this.currentLicenseKey = null;
		
		if (storage.getItem('user_license_key_list')) {
		    this.licenseKeyList = storage.getItem('user_license_key_list');
		}
		
		this.api = null;
		if (storage.getItem('user_data')) {
			this.refresh();
		}
	}
	
	
	get getUser() {
		return this.data;
	}
	
	refresh() {
		return this.signin(
			storage.getItem('user_data_license_key'),
			storage.getItem('user_data_password')
		);
	}
	
	signin(license_key, password) {
		this.api = new KeeperAPI(
            license_key,
			password
		);
		let res = this.api.apiCheckLicense();
		if (res.error) {
			this.logout();
			return res;
		}
		this.userIsAuth = true;
		this.data = storage.setItem('user_data', res.data);
		//оптимизация
		storage.setItem('user_data_license_key', license_key);
		storage.setItem('user_data_password', password);
		
		this.licenseKeyList[license_key] = {
		    'user_license_key': license_key,
		    'user_data_password': password,
		    'user_data': res.data
		};
		
		storage.setItem('user_license_key_list', this.licenseKeyList);
		
		
		return res;
	}
	
	logout() {
		this.userIsAuth = false;
		this.data = {};
		storage.removeItem('user_data');
		storage.removeItem('user_data_license_key'),
		storage.removeItem('user_data_password')
	}
	
	isAuth() {
		return this.userIsAuth;
	}
	
	getLogin() {
		return this.data.login;
	}
	
	getEmail() {
		return this.data.email;
	}
};