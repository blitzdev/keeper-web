class KeeperAPI {
	constructor(licenseKey, password, version) {
		this.version = version || "0.3";
		this.protocol = "";
		this.domain = "";
		this.params = {};
		this.params["license_key"] = licenseKey;
        this.params["hardware_key"] = "test";
        this.params["ip"] = "test";
		this.params["api_password"] = password;
        this.url = this.protocol + this.domain + 
		"/api/" + this.version + "/index.php";
		
	}
	
	
	postJsonRequest(url, data) {
		data = data || {};
		let opts = {
			type: 'POST',
			url: url,
			data: data,
			async: false
		}, response = null;
		try {
			response = $.ajax(opts).responseText;
			if (!response.trim()) {
			    return false;
			}
			console.log(JSON.parse(response));
			return JSON.parse(response);
		} catch(e) {
            console.log([response, e, opts, ]);
			Flash.development(JSON.stringify([response, e, opts, ]));
			return false;
		}
	}
	
	
	postJsonRequestAjax(url, method, data, callbackSuccess, callbackFail) {
        callbackSuccess = callbackSuccess || function () {};
        callbackFail = callbackFail || function () {};
        method = method || "GET";
        data = data || {};
        url = url || "";
        $.ajax({
            type: method,
            url: url,
            data: data,
            success: function (data) {
				//Flash.ok(JSON.stringify(data));
				if (data.error) {
				    callbackFail(data);
                    Flash.err(data.message);
					return;
				}
                callbackSuccess(data.data);
            },
            error: function (data) {
				//Flash.err(JSON.stringify(data));
                callbackFail(data);
                //Flash.err('Техническая ошибка запроса');
            }
        });
    }
	
	
	query(url, data) {
		data = data || {};
		$("#bm-preloader").show();
		let result = this.postJsonRequest(
			url, 
			this.objectMerge(data, this.params)
		);
		$("#bm-preloader").hide();
		if (result === false) {
			return this.messageError("Ошибка запроса к сервису!");
		}
		return result;
	}
	
	
	queryAjax(url, data, success, fail) {
		data = data || {};
		this.postJsonRequestAjax(
			url,
			"POST",
			this.objectMerge(data, this.params),
			success,
			fail
		);
	}
	
	
	//api functions
	apiCheckLicense(fields) {
		fields = fields || {};
		let func = "check_license";
		
		return this.query(this.url,
			this.objectMerge({
				"function" : func,
			}, fields)
		);
	}


	//получить список кошельков
    apiWalletGetListAjax(fields, success, fail) {
        fields = fields || {};
        let func = "wallet_get_list";

        this.queryAjax(this.url,
            this.objectMerge({
                "function" : func,
            }, fields),
            success, fail
        );
    }

	//получить баланс кошелька
    apiGetBalancesAjax(fields, success, fail) {
        fields = fields || {};
        let func = "wallet_get_balances";
        this.queryAjax(this.url,
            this.objectMerge({
                "function" : func,
            }, fields),
            success, fail
        );
    }

	//получить историю по кошельку
    apiGetHistoryListAjax(fields, success, fail) {
        fields = fields || {};
        let func = "wallet_get_history";
        this.queryAjax(this.url,
            this.objectMerge({
                "function" : func,
            }, fields),
            success, fail
        );
    }

	//выполнить перевод
    apiSendMoneyAjax(fields, success, fail) {
        fields = fields || {};
        let func = "wallet_sendmoney";
        this.queryAjax(this.url,
            this.objectMerge({
                "function" : func,
            }, fields),
            success, fail
        );
    }


    //удалить кошелек
    apiWalletDeleteAjax(fields, success, fail) {
        fields = fields || {};
        let func = "wallet_delete";
        this.queryAjax(this.url,
            this.objectMerge({
                "function" : func,
            }, fields),
            success, fail
        );
    }


    //добавить кошелек
    apiWalletAddAjax(fields, success, fail) {
        fields = fields || {};
        let func = "wallet_add";
        this.queryAjax(this.url,
            this.objectMerge({
                "function" : func,
            }, fields),
            success, fail
        );
    }


    //получить бесплатную лицензию
    apiGetFreeLicenseAjax(fields, success, fail) {
        fields = fields || {};
        let func = "get_free_license";
        this.queryAjax(this.url,
            this.objectMerge({
                "function" : func,
            }, fields),
            success, fail
        );
    }



	
	objectMerge(a, b) {
		return Object.assign(a, b);
	}
	
	messageSuccess(mes, data) {
		return {
			error: false,
			message: mes,
			data: data || {}
		};
	}
	
	messageError(mes, data) {
		return {
			error: true,
			message: mes,
			data: data || {}
		};
	}
}