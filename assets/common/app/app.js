	var
		storage = null,
		user = null, 
		app = null,
		projectName = 'Keeper';
		
	navigator && navigator.splashscreen && navigator.splashscreen.show();

	
	
	

    function onDeviceReady() {	
		
		//устанавливаем язык
		moment.locale('ru');

		
		//используем свое локальное хранилище
		storage = new JsonStorage();
		
		
		//инициализируем пользователя
		user = new User();
		
		router.beforeEach(function (to, from, next) {
			
			Flash.clear();
			
			if (app) {
				app.showMenu = false;
             	app.refresh();
			}
			
			/*runInitScripts();*/
			
			if (to.name.indexOf('app-pages-user-') === 0 && !user.isAuth()) {
				router.push({
					name: 'app-pages-login',
				});
				return;
			}
			if (user.isAuth() && (
				to.name.indexOf('app-pages-login') === 0 ||
				to.name.indexOf('app-pages-signup') === 0 ||
				to.name.indexOf('app-pages-forgot') === 0
			)) {
				router.push({
					name: 'app-pages-user-index',
				});
				return;		
			}
			if (user.isAuth() && to.name.indexOf('app-pages-exit') === 0) {
				user.logout();
				router.push({
					name: 'app-pages-index',
				});
				return;	
			}
			next();
		});

		Vue.nextTick(function () {
			setTimeout(function() {
				navigator && navigator.splashscreen && navigator.splashscreen.hide();
			}, 500);
		});
	
		app = new Vue({
			router,
			data: function () {
				return {
					user: user,
					flashOk: false,
					flashErr: false,
					flashDevelopment: false,
					development: true, //режим разработки
					showMenu: false,
					projectName: projectName,
					pageName: '',
                    version: '0.1.7.1',
					walletList: [],
				};
			},
			methods: {
				refresh: function () {
                    var vm = this;
                    if (user && user.api) {
                        user.api.apiWalletGetListAjax({}, function (data) {
                            vm.walletList = data.list;
                        }, function () {
                        });
                        
                        window.setInterval(function () {
                            user.api.queryAjax("/version.json", {}, function (data) {
                                if (vm.version != data.version) {
                                    sendNotify("Вышла новая версия, обновите страницу, чтобы использовать ее!", "Keeper - все кошельки с тобой!");
                                } 
                            }, function () {
                                
                            });
                        }, 30000);
                    }
				}
			},
		}).$mount('#app');


		app.refresh();
		
		/*runInitScripts();*/
	}
    onDeviceReady();
    
    
    
    document.addEventListener('DOMContentLoaded', function () {
        if (!Notification) {
            alert('Desktop notifications not available in your browser. Try Chromium.'); 
            return;
        }
        
        if (Notification.permission !== "granted") {
            Notification.requestPermission();
        }
    });

