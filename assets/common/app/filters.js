Vue.filter('formatCurrency', function (sum, currency) {
	sum = +sum;
	currency = currency || user.getCurrency();
	return formatCurrency(sum, currency);
});


Vue.filter('formatDate', function (date) {
	return formatDate(date);
});



Vue.filter('formatDateMonth', function (date) {
	return formatDateMonth(date);
});


Vue.filter('formatDateHours', function (date) {
	return formatDateHours(date);
});


Vue.filter('empty', function (value) {
	return value || '----';
});

Vue.filter('formatSystem', function (value) {
    return formatSystem(value);
});

