<!DOCTYPE html>
<html >
<head >
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Keeper WEB</title>
	<link href="/assets/common/css/bootstrap.min.css" rel="stylesheet" />
	<link href="/assets/common/css/material-kit.css" rel="stylesheet">
	<link href="/assets/common/css/style.css<?php echo "?".time(); ?>" rel="stylesheet">

	<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">

    <link href="assets/common/images/favicon.png" rel="shortcut icon">

    <script type="text/javascript">(window.Image ? (new Image()) : document.createElement('img')).src = 'https://vk.com/rtrg?p=VK-RTRG-154171-4MaGi';</script>


    <!-- Yandex.Metrika counter --> <script type="text/javascript" > (function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter45751968 = new Ya.Metrika({ id:45751968, clickmap:true, trackLinks:true, accurateTrackBounce:true, webvisor:true, trackHash:true }); } catch(e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = "https://cdn.jsdelivr.net/npm/yandex-metrica-watch/watch.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks"); </script> <noscript><div><img src="https://mc.yandex.ru/watch/45751968" style="position:absolute; left:-9999px;" alt="" /></div></noscript> <!-- /Yandex.Metrika counter -->
</head>

<body>
<div id="app">
        <div class="wrapper clearfix">
            <div class="content">
                <header v-if="user.isAuth()">
                    <div class="logo">
                        <router-link :to="{ name: 'app-pages-index' }">
                            <img src="/assets/common/images/keeper.png" alt="">
                        </router-link>
                    </div>
                    <ul class="main_menu">
                        <li>
                            <router-link :to="{ name: 'app-pages-user-index' }">
                                Кошельки
                            </router-link>
                        </li>
                        <li>
                            <router-link :to="{ name: 'app-pages-user-add-wallet' }">
                                Добавить кошелек
                            </router-link>
                        </li>
                    </ul>
                    <div class="user">
                        <router-link class="logout" style="margin-top: 7px;" :to="{ name: 'app-pages-exit' }">
                    </div>
                </header>
                <transition>

                    <router-view></router-view>
                </transition>
            </div>
        </div>
        <aside v-if="user.isAuth()">
            <div class="left_side">
                <div class="burger">
                    <a href="" class="button modal_click"><img src="/assets/common/images/burger.png" alt=""></a>
                </div>
                <div class="purses">
                    <p class="purse active">
                        <router-link :to="{ name: 'app-pages-index' }">
                            1<b>Keeper</b>
                        </router-link>
                    </p>
                </div>
            </div>
            <app-right-side user="user"></app-right-side>
        </aside>

        <!-- Navigation -->

        <div class="modal_block modal_сall" role="dialog" v-if="user.isAuth()">
            <p class="navigation">Навигация</p>
            <a href="" class="close_modal">
                <img src="/assets/common/images/close.png" alt="">
            </a>

            <ul class="main_menu">
                <li>
                    <router-link :to="{ name: 'app-pages-user-index' }">
                        Кошельки
                    </router-link>
                </li>
                <li>
                    <router-link :to="{ name: 'app-pages-user-add-wallet' }">
                        Добавить кошелек
                    </router-link>
                </li>
            </ul>
        </div>
    </div>

    <script src="/assets/common/js/jquery.min.js"></script>
    <script src="/assets/common/js/bootstrap.js"></script>
    <script src="/assets/common/js/material.min.js"></script>
    <script src="/assets/common/js/material-kit.js"></script>
    <script src="/assets/common/js/bootstrap-datepicker.js" type="text/javascript"></script>
    <script src="/assets/common/js/jquery.touchSwipe.min.js"></script>
    <script src="/assets/common/js/dropzone.js"></script>


    <script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>

    <script src="/assets/common/js/scripts.js"></script>



	<!-- BLOCK RIGHT SIDE -->
	<template id="app-right-side">
		<div class="right_side">
            <div v-for="item in walletList">
                <router-link :to="{ name: 'app-pages-user-wallet', params: { id: item.id, number: item.wallet, system: item.system } }" :class="{ payeer: item.system.toLowerCase() === 'payeer', 'perfect_money': item.system.toLowerCase() === 'perfectmoney', 'adv': item.system.toLowerCase() === 'advcash', 'qiwi': item.system.toLowerCase() === 'qiwi', 'name_purse': true, }">
                    <i><img :src="'/assets/common/images/' + item.system.toLowerCase() + '_add.png'" alt=""></i>
                    <div class="name_purse_text">
                        <b>{{ item.wallet }}</b>
                        <div class="for_align">
                            <p>{{ item.comment }}</p>
                        </div>
                    </div>
                </router-link>
            </div>
            <center v-if="walletList.length == 0" style="color: white;">
                <br><br>
                Ни один кошелек еще не добавлен
            </center>
		</div>
	</template>
	<!-- END BLOCK RIGHT SIDE -->



	<!-- BLOCK app-purse-card-info -->
	<template id="app-purse-card-info">
		<div >
			<p class="money" :class="{ 'balance-change-color': balanceChangeColor }" v-for="(balance, currency) in list">{{ balance | formatCurrency(currency) }} {{ currency }}</p>
			<div v-if="list.length == 0" style="color: white;">
				    <br><br>
				    <center>Загрузка балансов...</center>
			</div>
		</div>
	</template>
	<!-- END BLOCK app-purse-card-info -->
	
	<!-- PAGE LOGIN -->
	<template id="app-pages-login">

		<section class="entry">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
						<div class="entry_logo">
							<a href="#"><img src="/assets/common/images/logo.png" alt=""></a>
							<p>Единая система управления кошельками</p>
						</div>

						<form method="POST" action="" class="entry" v-on:submit.prevent="formLoginSubmit()">
							<div class="form-group">
								<input type="text" value="" placeholder="Ключ лицензии" class="form-control" v-model="licenseKey"/>
								<input type="submit" value="Войти">
                                <a style="cursor: pointer;" class="license" v-on:click.prevent="formGetFreeLicenseSubmit()">Получить лицензию</a>
							</div>
						</form>

					</div>
				</div>
			</div>
		</section>



	</template>
	<!-- END PAGE LOGIN -->

	
	<!-- PAGE USER INDEX -->
	<template id="app-pages-user-index">
		<section class="purse">
			<p class="hello">Добро пожаловать в Keeper</p>


			<div class="purse_cards">
				<div class="purse_card" :class="{ payeer: item.system.toLowerCase() === 'payeer', 'perfect_money': item.system.toLowerCase() === 'perfectmoney', 'adv': item.system.toLowerCase() === 'advcash', 'qiwi': item.system.toLowerCase() === 'qiwi'}" v-for="item in walletList">
					<div class="purse_title">
						<p>{{ item.wallet }}</p>
						<img :src="'/assets/common/images/' + item.system.toLowerCase() + '.png'" alt="">
					</div>

					<div class="function">
						<p>{{ item.comment }}</p>
					</div>

					<div class="purse_card_info">
						<p class="balance">Баланс кошелька</p>
						<app-purse-card-info :wallet-id="item.id" :wallet="item.wallet" :system="item.system"></app-purse-card-info>
					</div>

					<div class="buttons">
                        <router-link :to="{ name: 'app-pages-user-wallet', params: { id: item.id, number: item.wallet, system: item.system } }">
                            Войти<i> в кошелек</i>
                        </router-link>

                        <router-link class="delete" :to="{ name: 'app-pages-user-remove-wallet', params: { id: item.id } }">
                            Удалить
                        </router-link>


					</div>
				</div>
			</div>
		</section>


	</template>
	<!-- END PAGE USER INDEX -->




	<!-- PAGE USER WALLET -->
	<template id="app-pages-user-wallet">
		<section class="page_purse">
			<div class="balance">
				<p class="title">{{ system | formatSystem }} {{ wallet }}</p>

				<div class="balance_block" v-for="(amount, currency) in balanceList">
					<b>{{ currency }}</b>
					<p>{{ amount }}</p>
				</div>
				<div v-if="balanceList.length == 0" style="color: white;">
				    <br><br>
				    Загрузка балансов...
				</div>

				<div class="transfer">
					<p class="title tri" data-target="#transfer">Перевод средств</p>
					<form method="POST" action="" name="transfer" id="transfer" class="collapse in" v-on:submit.prevent="submitTransfer()">
						<div class="select">
							<label>Валюта</label>
							<div class="form-group">
								<select class="form-control" name="currency" v-model="currency">
									<option value="">--Валюта--</option>
									<option  :value="currency" v-for="(amount, currency) in balanceList">{{ amount }} {{ currency }}</option>
								</select>
								<span class="material-input"></span></div>
						</div>

						<div class="sum_transfer">
							<label>Сумма перевода</label>
							<div class="form-group is-empty"><input type="text" class="form-control" placeholder="Введите сумму" name="sum_transfer" v-model="amount"><span class="material-input"></span></div>
						</div>

						<div class="sum_write_off">
							<label>Куда</label>
							<div class="form-group is-empty"><input type="text" class="form-control" placeholder="Введите кошелек" name="sum_write_off" v-model="walletTo"><span class="material-input"></span></div>
						</div>

						<div class="comment">
							<label>Комментарий</label>
							<div class="form-group is-empty"><input type="text" class="form-control" placeholder="Комментарий" name="score" v-model="comment"><span class="material-input"></span></div>
						</div>

						<input type="submit" value="Перевести" :disabled="sendDisabled">
					</form>
				</div>

				<div class="history">
					<p class="title tri" data-target="#history">История операций</p>
					<br>

					<div class="table_transfer">
						<table>
							<tbody><tr>
								<td><p class="lab">ID транзакции</p></td>
								<td><p class="lab">Дата</p></td>
								<td><p class="lab">Сумма</p></td>
								<td><p class="lab">Кошелек</p></td>
							</tr>
							<tr v-for="item in historyList">
								<td><b>{{ item.transaction }}</b></td>
								<!--td></td-->
								<td><b>{{ item.date }}</b><i>{{ item.time }}</i></td>
								<td><b>
									<span>
									<img class="plus_plus" src="/assets/common/images/plus.png" alt="" v-if="item.type == 'income'">
									<img class="plus_minus" src="/assets/common/images/minus.png" alt="" v-else>
										{{ item.sum | formatCurrency(item.currency) }} {{ item.currency || formatCurrency(item.currency) }}
								</span>

								</b></td>
								<td><b>{{ item.account }}</b></td>
							</tr>

							<tr v-if="historyList.length == 0">
								<td colspan="4" style="color: white;text-align: center;">Нет записей</td>
							</tr>

							</tbody>
						</table>
					</div>
				</div>

			</div>
		</section>


	</template>
	<!-- END PAGE USER WALLET -->


    <!-- PAGE USER ADD WALLET -->
    <template id="app-pages-user-add-wallet">
        <section class="add_purse">
            <div class="add">
                <p class="title">Добавить кошелек</p>
                <form action="" name="add_purse" v-on:submit.prevent="submitAdd()">
                    <div class="select">
                        <label>Платежная система</label>
                        <select name="pay_system" v-model="system">
                            <option value="">--Выберите систему--</option>
                            <option :value="item.toLowerCase()" v-for="item in systemList">{{ item }}</option>
                        </select>
                    </div>

                    <div class="score">
                        <label>Номер счета</label>
                        <input type="text" class="form-control" placeholder="Введите счет кошелька" name="score" v-model="wallet">
                    </div>

                    <div class="api_id" v-if="system != 'qiwi'">
                        <label>API ID</label>
                        <input type="text" class="form-control" placeholder="Введите ID" name="api_id" v-model="api_id">
                    </div>

                    <div class="api_key">
                        <label v-if="system != 'perfectmoney'">API Key</label>
                        <label v-else>Пароль</label>
                        <input type="text" class="form-control" placeholder="Введите ключ API" name="api_key" v-model="api_key">
                    </div>

                    <div class="textarea">
                        <label>Комментарий</label>
                        <textarea class="form-control" placeholder="Введите комментарий" rows="3" v-model="comment"></textarea>
                    </div>

                </form>
                <div class="buttons">
                    <a class="add_btn" v-on:click="submitAdd()" style="cursor: pointer;" >Добавить</a>
                    <input type="reset" class="clear" value="Очистить" v-on:click="clear()">
                </div>

            </div>
        </section>
    </template>
    <!-- END PAGE USER ADD WALLET -->

	

	

	<!-- APPLICATION -->
    <script src="assets/common/app/libs.js<?php echo "?".time(); ?>"></script>
    <script src="assets/common/app/keeper-api.js<?php echo "?".time(); ?>"></script>
    <script src="assets/common/app/flash.js<?php echo "?".time(); ?>"></script>
    <script src="assets/common/app/json-storage.js<?php echo "?".time(); ?>"></script>

    <script src="assets/common/app/libs/vuejs-2.4.2/vue.js"></script>
    <script src="assets/common/app/libs/vue-router-2.7.0/vue-router.js"></script>

    <script src="assets/common/app/libs/moment.js/2.18.1/moment-with-locales.min.js"></script>

    <script src="assets/common/app/models/user.js<?php echo "?".time(); ?>"></script>

    <script src="assets/common/app/filters.js<?php echo "?".time(); ?>"></script>


    <script src="assets/common/app/pages/login.js<?php echo "?".time(); ?>"></script>
    <script src="assets/common/app/pages/user/index.js<?php echo "?".time(); ?>"></script>
    <script src="assets/common/app/pages/user/wallet.js<?php echo "?".time(); ?>"></script>
    <script src="assets/common/app/pages/user/remove-wallet.js<?php echo "?".time(); ?>"></script>
    <script src="assets/common/app/pages/user/add-wallet.js<?php echo "?".time(); ?>"></script>


    <script src="assets/common/app/components.js<?php echo "?".time(); ?>"></script>
    <script src="assets/common/app/routes.js<?php echo "?".time(); ?>"></script>


    <script src="assets/common/app/app.js<?php echo "?".time(); ?>"></script>
    
    <audio style="display: none;" id="audio-block">
        <source src="assets/common/audio/alert.mp3" type="audio/mpeg">
    </audio>
		
	<!-- END APPLICATION -->
</body>
</html>
