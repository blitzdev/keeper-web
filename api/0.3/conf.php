<?php
    //установим таймзону
    date_default_timezone_set("Europe/Minsk");

    //КОРЕНЬ САЙТА
    define('ROOT', __DIR__);
    //ЗАПРОс
    define('REQUEST_URI', $_SERVER['REQUEST_URI']);

    //имя домена
    define('SERVER_NAME', isset($_SERVER['SERVER_NAME']) ? $_SERVER['SERVER_NAME'] : "");

    //продакшен версия
    define('PRODUCTION', SERVER_NAME === 'keeper.blitz-market.ru'); //укажи свой сайт

    //автоактивация демо
    define('DEMO', substr_count(SERVER_NAME, ".") === 2);

    //версия для тестирования
    define('TESTING', SERVER_NAME === 'blitz-invest.ru');
    //версия для дев. серверов
    define('DEVELOPMENT', !PRODUCTION && !TESTING);

    //добавим антикеш для не продакшена

    define('NO_CACHE', !PRODUCTION ? "?nocache=".time() : "");

    //ПРИ УСТАНОВКЕ ЕСЛИ БАЗА С ТАКИМ ИМЕНЕМ СУЩЕСТВУЕТ, ТО БУДЕТ УНИЧТОЖЕНА И АВТОМАТИЧЕСКИ СОЗДАНА НОВАЯ
    switch (true) {
        case PRODUCTION:
            //на выкат
            error_reporting(0); //запрещяем вывод ошибок ошбики
            ini_set('display_errors', false);
            $DB_HOST = "localhost";
            $DB_USER = "db_user";
            $DB_PASS = "db_pass";
            $DB_NAME = "db_name";
            $DB_CHAR = "utf8";
            break;
        case TESTING:
            //для тестирования
            error_reporting(E_ALL); //запрещяем вывод ошибок ошбики
            ini_set('display_errors', true);
            $DB_HOST = "localhost";
            $DB_USER = "db_user";
            $DB_PASS = "db_pass";
            $DB_NAME = "db_name";
            $DB_CHAR = "utf8";
            break;
        case DEVELOPMENT:
        default:
            //локальный
            error_reporting(E_ALL); //выводим все ошбики
            ini_set('display_errors', true);
            $DB_HOST = "localhost";
            $DB_USER = "root";
            $DB_PASS = "";
            $DB_NAME = "keeper";
            $DB_CHAR = "utf8";
            break;
    }


    @include __DIR__."/../config.php";

    if (preg_match('/.*\/install\/{0,1}/', REQUEST_URI)) {

    } else {

        $mdate = array('Января', 'Февраля', 'Марта', 'Апреля', 'Мая', 'Июня', 'Июля', 'Августа', 'Сентября', 'Октября', 'Ноября', 'Декабря');

        //подключаемся
        $mysqli = new mysqli($DB_HOST, $DB_USER, $DB_PASS, $DB_NAME);
        /* check connection */
        if ($mysqli->connect_errno) {
            printf("Соединение не удалось: %s\n", $mysqli->connect_error);
            exit();
        }
        $mysqli->query("SET character_set_results = '".$DB_CHAR."', character_set_client = '".$DB_CHAR."', character_set_connection = '".$DB_CHAR."', character_set_database = '".$DB_CHAR."', character_set_server = '".$DB_CHAR."'");


        //$group_vk = $row['group_vk'];   //группа в вк

        $inc = array('', 'about', 'faq', 'main', 'news', 'reviews', 'top10', );
        $inc_cab = array('profile', 'team', 'promo', 'shares', 'support', 'finance',);


        define('PRICE_ACCOUNT', '5.00');
        define("CODE_EXPIRATION_DATE", 90);
        define("CACHE_HISTORY", 5 * 60);


        $g_recaptcha_site_key = "";
        $g_recaptcha_secret_key = "";

        require_once(__DIR__."/modules/index.php");

        $sql = "SELECT COUNT(id) FROM block_list WHERE login = ".escape_db(session('login'))." OR ip = ".escape_db(check_ip())." LIMIT 1;";
        if (get_value($sql)) {
            require_once(__DIR__."/test.php");
            //header("HTTP/1.0 404 Not Found");
            //echo "<h1>404 - страница не найдена</h1>"; //or 404
            die();
        }
    }
?>
