<?php

    function is_phone($phone) {
        $regex = "/^(\d[\s-]?)?[\(\[\s-]{0,2}?\d{3}[\)\]\s-]{0,2}?\d{3}[\s-]?\d{4}$/i";
        return $phone != "";//preg_match($regex, $phone);
    }

    function is_email($email) {
        return filter_var($email, FILTER_VALIDATE_EMAIL);
    }


    function is_url($url) {
        return filter_var($url, FILTER_VALIDATE_URL);
    }

    function is_payeer($number) {
        return preg_match("/P\d{6,}/", $number);
    }

    function is_perfectmoney($number) {
        return preg_match("/U{1,}+/", $number);
    }

    function is_nixmoney($number) {
        return preg_match("/U{1,}+/", $number);
    }

    function is_nixmoney_btc($number) {
        return preg_match("/B{1,}+/", $number);
    }

    function is_qiwi($number) {
        return is_phone($number);
    }

    function is_okpay($number) {
        return preg_match("/OK{1,}+/", $number);
    }

    function validate_check_payeer($number) {
        if (!is_payeer($number)) {
            $res['error'] = true;
            $res['message'] = "Введите корректный номер кошелька P123456(7)!";
        } else {
            $res['error'] = false;
            $res['message'] = "Кошелек введен корректно!";
        }
        return $res;
    }

    function validate_check_perfectmoney($number) {
        if (!is_perfectmoney($number)) {
            $res['error'] = true;
            $res['message'] = "Введите корректный номер кошелька U123456!";
        } else {
            $res['error'] = false;
            $res['message'] = "Кошелек введен корректно!";
        }
        return $res;
    }

    function validate_check_nixmoney($number) {
        if (!is_nixmoney($number)) {
            $res['error'] = true;
            $res['message'] = "Введите корректный номер кошелька U123456434343434!";
        } else {
            $res['error'] = false;
            $res['message'] = "Кошелек введен корректно!";
        }
        return $res;
    }

    function validate_check_nixmoney_btc($number) {
        if (!is_nixmoney_btc($number)) {
            $res['error'] = true;
            $res['message'] = "Введите корректный номер кошелька B123456434343434!";
        } else {
            $res['error'] = false;
            $res['message'] = "Кошелек введен корректно!";
        }
        return $res;
    }


    function validate_check_qiwi($number) {
        if (!is_qiwi($number)) {
            $res['error'] = true;
            $res['message'] = "Введите корректный номер кошелька +79111111111!";
        } else {
            $res['error'] = false;
            $res['message'] = "Кошелек введен корректно!";
        }
        return $res;
    }


    function validate_check_okpay($number) {
        if (!is_okpay($number)) {
            $res['error'] = true;
            $res['message'] = "Введите корректный номер кошелька OK9999999999!";
        } else {
            $res['error'] = false;
            $res['message'] = "Кошелек введен корректно!";
        }
        return $res;
    }

    function validate_check_password($password, $password_2) {
        $res['error'] = true;
        if (strlen($password) < 6) {
            $res['message'] = "Пароли должен быть более 5 символов в длину!";
            return $res;
        }
        if ($password !== $password_2) {
            $res['message'] = "Пароли не совпадают!";
            return $res;
        }
        $res['error'] = false;
        $res['message'] = "Пароли корректны!";
        return $res;
    }

    function validate_check_email($email) {
        if (!is_email($email)) {
            $res['error'] = true;
            $res['message'] = "Введите корректный email!";
        } else {
            $res['error'] = false;
            $res['message'] = "Почта введена корректно!";
        }
        return $res;
    }

    function validate_check_url($url) {
        if (!is_url($url)) {
            $res['error'] = true;
            $res['message'] = "Введите корректный URL!";
        } else {
            $res['error'] = false;
            $res['message'] = "Ошибка URL";
        }
        return $res;
    }

    function validate_check_login($login) {
        $res['error'] = true;
        if (strlen($login) < 6) {
            $res['message'] = "Логин должен быть длинее 5 символов!";
            return $res;
        }
        $res['error'] = false;
        $res['message'] = "Логин корректен!";
        return $res;
    }


    function validate_check_name($name) {
        $res['error'] = true;
        if (strlen($name) < 8) {
            $res['message'] = "ФИО должено быть длинее 8 символов!";
            return $res;
        }
        $res['error'] = false;
        $res['message'] = "ФИО корректно!";
        return $res;
    }
