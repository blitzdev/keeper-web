<?php
    //преобразовать строку xml в json массив
    function convert_xml_to_json($xml) {
        $xml = simplexml_load_string($xml);
        $json = json_encode($xml);
        return $json;
    }

    //преобразовать строку xml в  php массив
    function convert_xml_to_array($xml) {
        $json = convert_xml_to_json($xml);
        $array = json_decode($json, true);
        return $array;
    }

    //преобразовтаь кодировку с cp1251 в utf-8
    function convert_cp1251_to_utf8($str) {
        return iconv('windows-1251', 'UTF-8', $str);
    }

    //преобразовтаь кодировку с utf-8 в cp1251
    function convert_utf8_to_cp1251($str) {
        return iconv('UTF-8', 'windows-1251', $str);
    }
