<?php
    //получить название страны по системному коду
    function countries_get_title($name) {
        $row = get_row_by_field('countries', 'name', $name);
        if (!$row) {
            return false;
        }
        return $row['title'];
    }

    //получить название страны по системному коду
    function countries_get_title_en($name) {
        $row = get_row_by_field('countries', 'name', $name);
        if (!$row) {
            return false;
        }
        return $row['title_en'];
    }
