<?php

    function log_write($data) {
        ob_start();
        echo "Date: ";
        echo date("Y-m-d H:i:s");
        echo "\nResult:\n";
        var_dump($data);
        echo "\nEnd Result\n\n";
        $res = ob_get_clean();
        return file_put_contents("log.txt", $res, FILE_APPEND | LOCK_EX);
    }
