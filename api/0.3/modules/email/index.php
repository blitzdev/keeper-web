<?php
    function email_send_mail_reg_success($data) {
        global $name_project;
        $message = "";
        ob_start();
        extract($data);
        include(__DIR__."/templates/default.php");
        $message = ob_get_clean();
        $mail = new Mail("admin@".$_SERVER["SERVER_NAME"]);
        //$mail->setFrom("admin@".$_SERVER["SERVER_NAME"]);
        //$mail->setFromName("admin@".$_SERVER["SERVER_NAME"]);
        $mail->send($to, "Регистрация в {$name_project}", $message);
    }


    function email_send_mail_restore_code($data) {
        global $name_project;
        $message = "";
        ob_start();
        extract($data);
        include(__DIR__."/templates/restore.php");
        $message = ob_get_clean();
        $mail = new Mail("admin@".$_SERVER["SERVER_NAME"]);
        //$mail->setFrom("admin@".$_SERVER["SERVER_NAME"]);
        //$mail->setFromName("admin@".$_SERVER["SERVER_NAME"]);
        $mail->send($to, "Восстановление пароля в {$name_project}", $message);
    }
