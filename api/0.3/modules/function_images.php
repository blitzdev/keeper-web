<?php
    /*
        входной
        выходной
        ширина выходного
        высота выходного
        процент
    */
    function images_resize($file_input, $file_output, $w_o, $h_o, $percent = false) {
		if (!file_exists($file_input)) {
			echo ('<!--Файл не существует-->');
			return false;
		}
        list($w_i, $h_i, $type) = getimagesize($file_input);
        if (!$w_i || !$h_i) {
            echo ('<!--Невозможно получить длину и ширину изображения-->');
			return false;
        }
        $types = array('','gif','jpeg','png');
        $ext = $types[$type];
        if ($ext) {
            $func = 'imagecreatefrom'.$ext;
            $img = $func($file_input);
        } else {
            echo('<!--Некорректный формат файла-->');
			return false;
        }
        if ($percent) {
            $w_o *= $w_i / 100;
            $h_o *= $h_i / 100;
        }
        if (!$h_o) {
            $h_o = $w_o/($w_i/$h_i);
        }
        if (!$w_o) {
            $w_o = $h_o/($h_i/$w_i);
        }
        $img_o = imagecreatetruecolor($w_o, $h_o);
        imagecopyresampled($img_o, $img, 0, 0, 0, 0, $w_o, $h_o, $w_i, $h_i);
        if ($type == 2) {

            return imagejpeg($img_o,$file_output, 75);
        } else {
            $func = 'image'.$ext;
            return $func($img_o,$file_output);
        }
    }

    function images_crop($file_input, $file_output, $width, $height) {

        list($w_i, $h_i, $type) = getimagesize($file_input);
        if (!$w_i || !$h_i) {
            die('Невозможно получить длину и ширину изображения');
        }
        $types = array('','gif','jpeg','png');
        $ext = $types[$type];
        if ($ext) {
            $func = 'imagecreatefrom'.$ext;
            $img = $func($file_input);
        } else {
            die('Некорректный формат файла');
        }

        $delta_width = $w_i - $width;
        $delta_height = $h_i - $height;

        $x_o = $delta_width >> 1;
        $y_o = $delta_height >> 1;

        $w_o = $width;
        $h_o = $height;

        $img_o = imagecreatetruecolor($width, $height);
        imagecopy($img_o, $img, 0, 0, $x_o, $y_o, $w_o, $h_o);
        if ($type == 2) {
            return imagejpeg($img_o,$file_output, 65);
        } else {
            $func = 'image'.$ext;
            return $func($img_o,$file_output);
        }
    }

    //функции непостредственно для использования в коде мы просто передаем входное изображение и максимальные размеры

    function images_thumbnail_resize($input_image, $width, $height) {
        $cache_file_name = images_check_cache($input_image);
        //если залит новый файл и время кэширования истекло то повторно обрезаем
        if (!$cache_file_name) {
            $cache_file_name = images_get_cache_file_name($input_image);
            $result = images_resize(ROOT.$input_image, ROOT.$cache_file_name, $width, $height);
			if (!$result) {
				return false;
			}
            //images_crop($input_image, $cache_file_name, $width, $height);
            //images_crop($cache_file_name, $cache_file_name, $width, $height);
        }
        return $cache_file_name;
    }

    function images_check_cache($file_name) {
        $path_file = images_get_cache_file_name($file_name);
        $cache_time = 600;
        if (defined('CACHE_TIME')) {
            $cache_time = CACHE_TIME;
        }
        if (/*DEVELOPMENT || */(time() - @filemtime(ROOT.$path_file) > $cache_time)) {
            @unlink(ROOT.$path_file);
        } else {
            return $path_file;
        }
        return false;
    }

    function images_get_cache_file_name($file_name) {
        $cache_prefix = "cache_file__";
        $md5_file_name = md5($file_name);
        $cache_file_name = $cache_prefix.$md5_file_name;
        $cache_extension = ".".substr(strrchr($file_name,'.'), 1);

        $cache_dir = "/cache/";
        if (!is_dir(ROOT.$cache_dir)) {
            mkdir(ROOT.$cache_dir);
        }

        return $cache_dir.$cache_file_name.$cache_extension;
    }

    function images_get_type($file) {
        $size = getimagesize($file);
        switch ($size["mime"]) {
            case "image/jpeg":
                return "jpeg";
            case "image/gif":
                return "gif";
            case "image/png":
                return "png";
        }
        return false;
    }
