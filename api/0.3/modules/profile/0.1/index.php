<?php
    //получить фото профиля
    function profile_get_photo($login) {
        static $profile_cache_photo = [];
        $w = 132;
        $h = 132;
        if (!isset($profile_cache_photo[$login])) {
            $res = false;
            $user = get_row_by_field('users', 'login', $login);
            if (!$user['img']) {
                $res = images_thumbnail_resize("/assets/common/img/avatar.jpg", $w, $h);
            } else {
                $res = images_thumbnail_resize($user['img'], $w, $h);
            }
            $profile_cache_photo[$login] = $res;
        }
        return $profile_cache_photo[$login];
    }


    //получить фио
    function profile_get_name($login) {
        static $profile_cache_name = [];
        if (!isset($profile_cache_name[$login])) {
            $res = false;
            $user = get_row_by_field('users', 'login', $login);
            if (!empty($user['name']) &&  !empty($user['lastname'])) {
                $res = $user['name']." ".$user['lastname'];
            } else {
                $res = $user['login'];
            }
            $profile_cache_name[$login] = $res;
        }
        return $profile_cache_name[$login];
    }
