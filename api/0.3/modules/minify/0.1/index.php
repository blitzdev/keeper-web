<?php
	include_once(__DIR__.'/min/lib/Minify/HTML.php');
    include_once(__DIR__.'/min/lib/Minify/CSS.php');
    include_once(__DIR__.'/min/lib/JSMin.php');

	function minify_content($data) {
		$data = Minify_HTML::minify($data, array(
			'cssMinifier' => array('cache', '/assets/common/css'),
			'jsMinifier' => array('cache', '/assets/common/js')
		));
		return $data;
	}
