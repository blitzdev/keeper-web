<?php

    if (!class_exists('Mail')) {
        class Mail {

          private $from;
          private $from_name = "";
          private $type = "text/html";
          private $encoding = "utf-8";
          private $notify = false;

          /* Конструктор принимающий обратный e-mail адрес */
          public function __construct($from) {
            $this->from = $from;
          }

          /* Изменение обратного e-mail адреса */
          public function setFrom($from) {
            $this->from = $from;
          }

          /* Изменение имени в обратном адресе */
          public function setFromName($from_name) {
            $this->from_name = $from_name;
          }

          /* Изменение типа содержимого письма */
          public function setType($type) {
            $this->type = $type;
          }

          /* Нужно ли запрашивать подтверждение письма */
          public function setNotify($notify) {
            $this->notify = $notify;
          }

          /* Изменение кодировки письма */
          public function setEncoding($encoding) {
            $this->encoding = $encoding;
          }

          /* Метод отправки письма */
          public function send($to, $subject, $message) {
            ini_set('sendmail_from', $this->from);
            $from = "=?utf-8?B?".base64_encode($this->from_name)."?="." <".$this->from.">"; // Кодируем обратный адрес (во избежание проблем с кодировкой)
            $headers = /*"From: ".$from."\r\nReply-To: ".$from."\r\n*/"Content-type: ".$this->type."; charset=".$this->encoding."\r\n"; // Устанавливаем необходимые заголовки письма
            if ($this->notify) $headers .= "Disposition-Notification-To: ".$this->from."\r\n"; // Добавляем запрос подтверждения получения письма, если требуется
            $subject = "=?utf-8?B?".base64_encode($subject)."?="; // Кодируем тему (во избежание проблем с кодировкой)
            return mail($to, $subject, $message, $headers); // Отправляем письмо и возвращаем результат
          }

        }
    }
    //следует использовать для защиты от xss
    function escape($value) {
        return htmlentities($value, ENT_QUOTES, "UTF-8");
    }
    //следует использовать если команды для подключения файлов, удаления, создания и прочего приходят от пользователя
    function escape_fs($value) {
        return escapeshellarg($value);
    }

    function escape_db($value) {
        global $mysqli;
        return "'".$mysqli->real_escape_string($value)."'";
    }


    function format_money($money) {
        return sprintf("%01.2f", $money);
    }

    function format_number($number) {
        return format_Money($number);
    }

    function format_date($date) {
        return date('Y-m-d H:i:s', (int)$date);
    }



    //это пост запрос
    function is_post($action_name=false) {
        if (!$action_name) {
            return $_SERVER['REQUEST_METHOD'] === 'POST';
        }
        return $_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST[$action_name]);
    }


    function is_get($action_name=false) {
        if (!$action_name) {
            return $_SERVER['REQUEST_METHOD'] === 'GET';
        }
        return $_SERVER['REQUEST_METHOD'] === 'GET' && isset($_POST[$action_name]);
    }

    //это ajax запрос
    function is_ajax() {
        return !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest';
    }

    function redirect($link) {
        header("Location: ".$link, TRUE, 301);
        exit(0);
    }

    function send_json($data) {
        header('Content-Type: application/json');
        echo json_encode($data);
        exit(0);
    }

    function send_json_message($message, $data=[]) {
        send_json([
            'error' => false,
            'message' => $message,
            'data' => $data,
        ]);
    }

    function throw_json_error($message, $data=[]) {
        send_json([
            'error' => true,
            'message' => $message,
            'data' => $data,
        ]);
    }


    function post($key, $value=false) {
        if ($value) {
            $_POST[$key] = $value;
        }
        return isset($_POST[(string)$key]) ? $_POST[(string)$key] : "";
    }

    function get($key, $value=false) {
        if ($value) {
            $_GET[$key] = $value;
        }
        return isset($_GET[(string)$key]) ? $_GET[(string)$key] : "";
    }

    function request($key, $value=false) {
        if ($value) {
            $_REQUEST[$key] = $value;
        }
        return isset($_REQUEST[(string)$key]) ? $_REQUEST[(string)$key] : "";
    }

    function session($key, $value=false) {
        if ($value) {
            $_SESSION[$key] = $value;
        }
        return isset($_SESSION[(string)$key]) ? $_SESSION[(string)$key] : "";
    }

    function server($key, $value=false) {
        if ($value) {
            $_SERVER[$key] = $value;
        }
        return isset($_SERVER[(string)$key]) ? $_SERVER[(string)$key] : "";
    }

    function cookie($key, $value=false) {
        if ($value) {
            $_COOKIE[$key] = $value;
        }
        return isset($_COOKIE[(string)$key]) ? $_COOKIE[(string)$key] : "";
    }

    function V($key) {
        return isset($key) ? $key : "";
    }

    //получить строку
    function get_row_by_id($table, $id) {
        global $mysqli;
        $list = [];
        $sql = "SELECT * FROM {$table} WHERE id = ".(int)$id." LIMIT 1;";
        $result = $mysqli->query($sql);
        if ($result) {
            return $result->fetch_assoc();
        }
        return [];
    }

    //получить значение
    function get_value_by_id($table, $id, $field) {
        global $mysqli;
        $list = [];
        $sql = "SELECT {$field} FROM {$table} WHERE id = ".(int)$id." LIMIT 1;";
        $result = $mysqli->query($sql);
        if ($result) {
            return $result->fetch_array()[0];
        }
        return [];
    }

    //получить строку
    function get_row_by_field($table, $field, $value) {
        global $mysqli;
        $list = [];
        $sql = "SELECT * FROM `{$table}` WHERE `{$field}` = ".escape_db($value)." LIMIT 1;";
        $result = $mysqli->query($sql);
        if ($result) {
            return $result->fetch_assoc();
        }
        return [];
    }


    //получить строку по входждению подстроки
    function get_row_by_like_field($table, $field, $value) {
        global $mysqli;
        $list = [];
        $value = $mysqli->real_escape_string($value);
        $sql = "SELECT * FROM `{$table}` WHERE `{$field}` LIKE '%{$value}%' LIMIT 1;";
        $result = $mysqli->query($sql);
        if ($result) {
            return $result->fetch_assoc();
        }
        return [];
    }

    //получить строку по таргету
    function get_row_by_target($table, $value) {
        global $mysqli;
        $list = [];
        $value = $mysqli->real_escape_string($value);
        $sql = "SELECT * FROM `{$table}` WHERE LOWER(`target`) LIKE LOWER('%{$value}%') LIMIT 1;";
        $result = $mysqli->query($sql);
        if ($result) {
            return $result->fetch_assoc();
        }
        return [];
    }

    //получить максимальный id
    function get_max_id($table) {
        global $mysqli;
        $sql = "SELECT MAX(id) as max FROM `{$table}` LIMIT 1;";
        $res = $mysqli->query($sql);
        if ($res) {
            if($row = $res->fetch_assoc()) {
                return (int)$row['max'];
            }
        }
        return 0;
    }

    //получить таблицу, упорядочить по полю, выбрать если
    function get_list($table, $where="1 = 1", $order='id') {
        global $mysqli;
        $list = [];
        $sql = "SELECT * FROM `{$table}` ";
        $sql .= " WHERE ".$where." ";
        $sql .= " ORDER BY {$order} ";
        $sql .= " ;";
        $result = $mysqli->query($sql);
        if ($result) {
            while ($row = $result->fetch_assoc()) {
                $list[] = $row;
            }
        }
        return $list;
    }

    function get_value($sql) {
        global $mysqli;
        $value = 0;
        $result = $mysqli->query($sql);
        if ($result) {
            $row = $result->fetch_array();
            if ($row && isset($row[0])) {
                return $row[0];
            }
        }
        return $value;
    }

    function get_site_url() {
      return sprintf(
        "%s://%s/",
        isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https' : 'http',
        $_SERVER['SERVER_NAME']
      );
    }


    function install_re_captcha_2($sitekey) {
        echo '<div class="g-recaptcha" data-sitekey="'.$sitekey.'"></div>';
        echo "<script src='https://www.google.com/recaptcha/api.js'></script>";
    }

    function check_re_captcha_2($secret) {
        $data = [];
        $data['secret'] = $secret;
        $data['response'] = isset($_REQUEST['g-recaptcha-response']) ? $_REQUEST['g-recaptcha-response'] : "";
        $result = get_post_request("https://www.google.com/recaptcha/api/siteverify", $data);
        return isset($result['success']) ? $result['success'] === true : false;
    }

    function get_post_request($url='', $data=[], $agent='Mozilla/5.0 (Windows NT 6.1; rv:12.0) Gecko/20100101 Firefox/12.0') {
        if (!function_exists('curl_init')) {
            die('curl library not installed');
            return false;
        }
        $curl = curl_init();
        curl_setopt_array($curl, [
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => http_build_query($data),
            CURLOPT_HEADER => 0,
            CURLOPT_SSL_VERIFYPEER => 0,
            CURLOPT_SSL_VERIFYHOST => 0,
            CURLOPT_USERAGENT => $agent,
        ]);
        $response = curl_exec($curl);
        curl_close($curl);
        return json_decode($response, true);
    }


    function get_new_password() {
        $pass = hash('ripemd160', uniqid(rand(),true));
        return substr($pass, 0, 8);
    }

    function send_restore_message($user, $newpass) {
        global $admin_email;
        global $name_project;
        $login = $user['login'];
        $email = $user['email'];
        $mail = new Mail($admin_email);
        $subject = "Ваши данные";
        $message = "Ваши данные от личного кабинета ".$name_project."<br>";
        $message .= "Логин: ".escape($login)."<br>";
        $message .= "Пароль: ".$newpass;
        $mail->send($email, $subject, $message);
    }

    function dbEscape($value) {
        global $mysqli;
        return "'".$mysqli->real_escape_string($value)."'";
    }

    function formatMoney($money) {
        return sprintf("%01.2f", $money);
    }

    function formatNumber($number) {
        return formatMoney($number);
    }

    function formatDate($date) {
        return date('Y-m-d H:i:s', $date);
    }

        //следует использовать если команды для подключения файлов, удаления, создания и прочего приходят от пользователя
    function fsEscape($value) {
        return escapeshellarg($value);
    }



    //это пост запрос
    function isPost() {
        return $_SERVER['REQUEST_METHOD'] === 'POST';
    }

    //это ajax запрос
    function isAjax() {
        return !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest';
    }


    //статус платежа
    function get_status($status) {
        return $status == 1 ? "<div style='color: green;'>Успешно</div>" : "<div style='color: red;'>Невыполнено</div>";
    }

        //статус платежа
    function get_depozit_status($status) {
        return $status == 1 ? "<div style='color: green;'>Отработал</div>" : "<div style='color: red;'>В процессе</div>";
    }

    //статус платежа
    function get_bool_status($status) {
        return $status != 1 ? "<div style='color: green;'>Включена</div>" : "<div style='color: red;'>Выключена</div>";
    }

    //добавить вклад
    function activate_invest($order_id, $summa) {
        return false;
    }

    function sendJson($data) {
        header('Content-Type: application/json');
        echo json_encode($data);
        exit(0);
    }

    function throwJsonError($message) {
        sendJson([
            'error' => true,
            'message' => $message
        ]);
    }

    function show_js_alert($message) {
        ?><script>window.alert("<?php echo $message; ?>");</script><?php
    }
