<?php
    /* В версии 0.2 добавлен срок жизни для csrf токена */
    define("CSRF_EXPIRATION_DATE", 15 * 60); //15 минут
    //получить csrf token
    function csrf_get_token() {
        static $call = false;
        if (!session('csrf_token') || $call === false) {
            $call = true;
            $token = md5(uniqid(rand(), true));
            session("csrf_token_expiration_date", time() + CSRF_EXPIRATION_DATE);
            return session('csrf_token', $token);
        }
        return session('csrf_token');
    }

    //пороверить токен в соответствии с запросом(должна вызываться раньше csrf_get_token)
    function csrf_check_token() {
        if (session("csrf_token_expiration_date") - time() <= 0) {
            die("Срок действия CSRF токена истек!");
        }
        if (is_post() && post('csrf') === session('csrf_token')) {
            return true;
        }
        if (is_get() && get('csrf') === session('csrf_token')) {
            return true;
        }
        die("Неправильный CSRF - токен!");
        return false;
    }

    //установить токен
    function csrf_install() {
        $token = csrf_get_token();
        ob_start();
        ?>
            <input type="hidden" name="csrf" value="<?php echo $token; ?>">
        <?php
        return ob_get_clean();
    }
