<?php

    function make_message($message="") {
        return [
            'error' => false,
            'message' => $message,
        ];
    }

    function make_error($message="") {
        return [
            'error' => true,
            'message' => $message,
        ];
    }

    function make_data($data, $message="") {
        return [
            'error' => false,
            'message' => $message,
            'data' => $data,
        ];
    }
