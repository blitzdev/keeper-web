<?php

  function generate_code($length = 7) {
      $num = range(0, 9);
      $alf = range('a', 'z');
      $_alf = range('A', 'Z');
      $symbols = array_merge($num, $alf, $_alf);
      shuffle($symbols);
      $code_array = array_slice($symbols, 0, (int)$length);
      $code = implode("", $code_array);
      return $code;
  }
