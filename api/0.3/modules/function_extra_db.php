<?php

	//�������� ������� ���� ������
	function extra_db_get_current_db_name($db_name=false) {
		if ($db_name) {
			return $db_name;
		}
		if (defined("DB_NAME")) {
			return DB_NAME;
		}
		global $DB_NAME;
		if (isset($DB_NAME)) {
			return $DB_NAME;
		}
		return $db_name;
	}

	//�������� ���������� ������� � ���� ������
	function extra_db_get_handle_db($handle=false) {
		if ($handle) {
			return $handle;
		}
		global $mysqli;
		if (isset($mysqli)) {
			return $mysqli;
		}
		return $handle;
	}

	//�������� ������ ������
	function extra_db_get_tables($db_name=false, $handle=false) {
		$mysqli = extra_db_get_handle_db($handle);
		$db_name = extra_db_get_current_db_name($db_name);
		$list = [];
		$sql = "SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES";
		$sql .= " WHERE TABLE_TYPE = 'BASE TABLE' ";
		$sql .= "AND TABLE_SCHEMA = '".$mysqli->real_escape_string($db_name)."'";
        $result = $mysqli->query($sql);
        if ($result) {
            while ($row = $result->fetch_assoc()) {
                $list[] = $row["TABLE_NAME"];
            }
        }
        return $list;
	}

	//�������� ���������� � ��������
	function extra_db_get_info_columns($db_table_name=false, $db_name=false, $handle=false) {
		$mysqli = extra_db_get_handle_db($handle);
		$db_name = extra_db_get_current_db_name($db_name);
		$list = [];
		$sql = "SELECT * FROM INFORMATION_SCHEMA.COLUMNS ";
		$sql .= "WHERE TABLE_NAME = '".$mysqli->real_escape_string($db_table_name)."'";
		$sql .= " AND TABLE_SCHEMA = '".$mysqli->real_escape_string($db_name)."'";
        $result = $mysqli->query($sql);
        if ($result) {
            while ($row = $result->fetch_assoc()) {
                $list[] = $row;
            }
        }
        return $list;
	}

    //�������� �������� ������� ��������������
    function extra_db_get_autoincrement_value($db_table_name=false, $db_name=false, $handle=false) {
        $mysqli = extra_db_get_handle_db($handle);
        $db_name = extra_db_get_current_db_name($db_name);
        $sql = "SELECT `AUTO_INCREMENT` FROM INFORMATION_SCHEMA.TABLES ";
        $sql .= "WHERE TABLE_NAME = '".$mysqli->real_escape_string($db_table_name)."'";
		$sql .= " AND TABLE_SCHEMA = '".$mysqli->real_escape_string($db_name)."'";
        return get_value($sql);
    }



    //получить таблицу, упорядочить по полю, выбрать если
    function extra_db_get_list($table, $where=false, $order=false, $fields="*", $group_by=false, $having=false, $join=false) {
        global $mysqli;
        $list = [];
        $sql = "SELECT {$fields} FROM `{$table}` ";
        if ($where) {
            $sql .= " WHERE {$where} ";
        }
        //join требует полной записи
        if ($join) {
            $sql .= " {$join} ";
        }
        if ($group_by) {
            $sql .= " GROUP BY {$group_by} ";
        }
        if ($having) {
            $sql .= " HAVING {$having} ";
        }
        if ($order) {
            $sql .= " ORDER BY {$order} ";
        }
        $sql .= " ;";
        $result = $mysqli->query($sql);
        if ($result) {
            while ($row = $result->fetch_assoc()) {
                $list[] = $row;
            }
        }
        return $list;
    }
