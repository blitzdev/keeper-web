<?php

    function is_guest() {
        return !is_user();
    }

    function is_user() {
        return (bool)session('id');
    }

    function logout() {
        session_unset();
    }

    function login() {
        global $mysqli;
        $user = get_row_by_field('users', "login", session('auth_login'));
        
        foreach ($user as $key=>$value) {
            $_SESSION[$key] = $value;
        }
        unset($_SESSION['auth_login']);

		$login = $user['login'];
        $time = time();
        $ip = check_ip();
        $mysqli->query("INSERT INTO `visit` (login, date, ip) VALUES ('$login', '$time', '$ip')");
        $sql = "UPDATE `users` SET `last_auth` = ".escape_db($time)." WHERE `login` = ".escape_db(session('auth_login')).";";
        $mysqli->query($sql);

        return true;
    }

    function is_auth() {
        return (bool)session('id');
    }


    function update_login() {
        $user = get_row_by_field('users', "login", session('login'));
        foreach ($user as $key=>$value) {
            $_SESSION[$key] = $value;
        }
    }
