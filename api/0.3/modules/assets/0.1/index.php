<?php

    $ASSETS_DIR = "/assets/";

    function assets_add_script($name, $dir="") {
        global $ASSETS_DIR;
        if ($dir) {
            $dir = "{$dir}/";
        }
        $path = "{$ASSETS_DIR}{$dir}js/{$name}.js";
        ?><script src="<?php echo $path; ?>"></script>
        <?php
    }


    function assets_add_style($name, $dir="") {
        global $ASSETS_DIR;
        if ($dir) {
            $dir = "{$dir}/";
        }
        $path = "{$ASSETS_DIR}{$dir}css/{$name}.css";
        ?><link rel="stylesheet" href="<?php echo $path; ?>" />
        <?php
    }

    function assets_add_scripts($names, $dir="") {
        foreach ($names as $name) {
            assets_add_script($name, $dir);
        }
    }


    function assets_add_styles($names, $dir="") {
        foreach ($names as $name) {
            assets_add_style($name, $dir);
        }
    }
