<?php
    require_once(__DIR__."/libs/MerchantWebService.php");



    //получение баланса
    function advcash__get_balance($wallet, $api_id, $api_key) {
        $merchantWebService = new MerchantWebService();

        $arg0 = new authDTO();
        $arg0->apiName = $api_id;
        $arg0->accountEmail = $wallet;
        $arg0->authenticationToken = $merchantWebService->getAuthenticationToken($api_key);

        $getBalances = new getBalances();
        $getBalances->arg0 = $arg0;

        $res = [];
        $list = [];
        try {
            $getBalancesResponse = $merchantWebService->getBalances($getBalances);
            foreach ($getBalancesResponse->return as $item) {
                if (substr($item->id, 0, 1) === "U") {
                    if (!isset($list["usd"])) {
                        $list["usd"] = 0;
                    }
                    $list["usd"] += (float)$item->amount;
                } else if (substr($item->id, 0, 1) === "E") {
                    if (!isset($list["eur"])) {
                        $list["eur"] = 0;
                    }
                    $list["eur"] += (float)$item->amount;
                } else if (substr($item->id, 0, 1) === "R") {
                    if (!isset($list["rub"])) {
                        $list["rub"] = 0;
                    }
                    $list["rub"] += (float)$item->amount;
                }
            }
            $res = message_success("", [
                "list" => $list,
            ]);
        } catch (Exception $e) {
            $res = message_error("Ошибка извлечения баланса! ".$e->getMessage());
        }
        return $res;
    }


    //проверка авторизации
    function advcash__is_valid($wallet, $api_id, $api_key) {
        $res = advcash__get_balance($wallet, $api_id, $api_key);
        if (!$res["error"]) {
            $res = message_success("Вы успешно авторизовались!");
        }
        return $res;
    }


    //получить историю
    function advcash__get_history($wallet, $api_id, $api_key, $date_start=false, $date_end=false) {
        if (!$date_start) {
            $date_start = strtotime("-7 days");
        }
        if (!$date_end) {
            $date_end = time();
        }
        if ($date_end - $date_start > 3 * 86400) {
            $date_start = $date_end - 3 * 86400;
        }

        $merchantWebService = new MerchantWebService();

        $arg0 = new authDTO();
        $arg0->apiName = $api_id;
        $arg0->accountEmail = $wallet;
        $arg0->authenticationToken = $merchantWebService->getAuthenticationToken($api_key);

        $arg1 = new merchantAPITransactionFilter();
        $arg1->startTimeFrom = date(DATE_ATOM, $date_start);
        //$arg1->startTimeTo = date(DATE_ATOM, $date_end);
//$arg1->from = 100;

        $arg1->sortOrder = 'ASC';
        $arg1->count = 1000;

        $history = new history();
        $history->arg0 = $arg0;
        $history->arg1 = $arg1;

        $res = [];
        try {
            $historyResponse = $merchantWebService->history($history);
            foreach ($historyResponse->return as $obj) {
                $item = [];
                $currency = strtolower($obj->currency);
                if ($currency === 'rur') {
                    $currency = "rub";
                }
                $date = $obj->startTime;
                $date = strtotime($date);
                $item["transaction"] = $obj->id;
                $item["date"] = date("d.m.Y", $date);
                $item['time'] = date("H:i:s", $date);
                $item["datetime"] = $item["date"]." ".$item['time'];
                $item['sum'] = $obj->amount;
                $item['currency'] = $currency;
                $item['comment'] = (string)$obj->comment;
                $item['type'] = $obj->direction === "INCOMING" ? "income" : "expenditure";
                if ($obj->status === "COMPLETED") {
                    $item["status"] = "success";
                } else if ($obj->status === "PROCESS" || $obj->status === "PENDING") {
                    $item["status"] = "precessed";
                } else {
                    $item["status"] = "error";
                }
                $item["account"] = $obj->walletSrcId;
                $item['provider'] = "AdvCash";
                $item['commission_sum'] = $obj->fullCommission;
                $item['commission_currency'] = $currency;

                //$res[] = $item;
                array_unshift($res, $item);
            }
            return message_success("", [
            	"list" => $res,
            ]);
        } catch (Exception $e) {
            return message_error("Ошибка извлечения баланса! ".$e->getMessage());
        }
        return message_error("Ошибка #1");
    }



    //отправить деньги
    function advcash__payment($wallet, $api_id, $api_key, $number, $amount, $currency="rub", $comment="") {
        if ($currency === "rub") {
            $currency = "rur";
        }
        $currency = strtoupper($currency);

        $merchantWebService = new MerchantWebService();
        $arg0 = new authDTO();
        $arg0->apiName = $api_id;
        $arg0->accountEmail = $wallet;
        $arg0->authenticationToken = $merchantWebService->getAuthenticationToken($api_key);

        $arg1 = new sendMoneyRequest();
        $arg1->amount = (float)$amount;

        $arg1->currency = $currency;


        $first_letter = substr($number, 0, 1);

        if ($first_letter === "U" || $first_letter === "E" || $first_letter === "R") {
            $arg1->walletId = $number;
        } else {
            $arg1->email = $number;
        }
        $arg1->note = $comment;
        $arg1->savePaymentTemplate = false;

        $validationSendMoney = new validationSendMoney();
        $validationSendMoney->arg0 = $arg0;
        $validationSendMoney->arg1 = $arg1;

        $sendMoney = new sendMoney();
        $sendMoney->arg0 = $arg0;
        $sendMoney->arg1 = $arg1;

        $res = [];
        try {
            $merchantWebService->validationSendMoney($validationSendMoney);
            $sendMoneyResponse = $merchantWebService->sendMoney($sendMoney);

            $res = message_success("Выплата осуществлена!", [
                "id" => $sendMoneyResponse->return,
                "sms" => false,
            ]);
        } catch (Exception $e) {
            $res = message_error($e->getMessage());
        }

        return $res;
    }
