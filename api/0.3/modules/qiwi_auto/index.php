<?php
    require_once 'Curl.php';
    require_once 'CDom.php';
    require_once('simple_html_dom.php');

    use \Curl\Curl;

    function get_sum_transaction_by_qiwi($phone, $password, $code) {


        $curl = new Curl();

        $curl->setOpt(CURLOPT_SSL_VERIFYPEER, 0);

        $curl->setCookieFile('cookie.txt');
        $curl->setHeader('Content-Type', 'application/json');
        $curl->setHeader('X-Requested-With', 'XMLHttpRequest');


        $data = [
            'login' => $phone,
            'password' => $password,
        ];

        /*$res = $curl->post('https://visa.qiwi.com/person/state.action');
        if (isset($res->data->balances->RUB)) {
            echo "Balance 1 = ".$res->data->balances->RUB;
            exit(0);
        }*/


        $res = $curl->post('https://auth.qiwi.com/cas/tgts', json_encode($data));


        $TICKET = $res->entity->ticket;

        $data = [
            "ticket" => $TICKET,
            "service" => "https://qiwi.com/j_spring_cas_security_check",
        ];

        $res = $curl->post('https://auth.qiwi.com/cas/sts', json_encode($data));


        $data = [
            "ticket" => $TICKET,
            "service" => "https://qiwi.com/j_spring_cas_security_check",
        ];


        $res = $curl->post('https://auth.qiwi.com/cas/sts', json_encode($data));


        $TICKET = $res->entity->ticket;

        $data = [
            "ticket" => $TICKET,
        ];

        $res = $curl->get('https://qiwi.com/j_spring_cas_security_check?ticket='.$TICKET);


        //$res = $curl->get('https://visa.qiwi.com/report/list.action?type=2');

        //$res = $curl->post('https://visa.qiwi.com/person/state.action');

        // if (isset($res->data->balances->RUB)) {
        //     echo "Balance 2 =".$res->data->balances->RUB;
        //     exit(0);
        // } else {
        //     die('Ошибка');
        // }

        $res = $curl->get('https://qiwi.com/report/list.action?type=3');


        //https://github.com/amal/CDom
        $dom = CDom::fromString($res);

        if (!function_exists("mb_str_replace")) {
            function mb_str_replace($needle, $replacement, $haystack) {
                return implode($replacement, mb_split($needle, $haystack));
            }
        }

        //выбираем все успешные транзакции
        foreach($dom->find('div.reportsLine.status_SUCCESS') as $element) {
                $summa = trim($element->find('div.IncomeWithExpend.income')->find('div.cash'));
                $transaction = trim($element->find('div.transaction'));
                if ($summa == "") {
                    continue;
                }
                if (mb_strpos($summa, 'руб.' ) === false) {
                    continue;
                }
                if ($transaction !== $code) {
                    continue;
                }
                $summa = mb_str_replace("руб.", "", $summa);
                $summa = mb_str_replace("&nbsp;", "", $summa);
                $summa = mb_str_replace("\s", "", $summa);
                $summa = mb_str_replace(",", ".", $summa);
                $summa = trim($summa);
                return format_number($summa);
        }
        return false;
}



        function payment_qiwi($phone, $password, $to, $sum, $comment = "") {
            $curl = new Curl();

            $curl->setOpt(CURLOPT_SSL_VERIFYPEER, 0);

            $curl->setCookieFile($phone);

            $curl->setHeader('Content-Type', 'application/json');
            $curl->setHeader('X-Requested-With', 'XMLHttpRequest');


            $data = [
                'login' => $phone,
                'password' => $password,
            ];

            $res = $curl->post('https://auth.qiwi.com/cas/tgts', json_encode($data));

            $TICKET = $res->entity->ticket;


            $data = [
                "ticket" => $TICKET,
                "service" => "https://qiwi.com/j_spring_cas_security_check",
            ];

            $res = $curl->post('https://auth.qiwi.com/cas/sts', json_encode($data));


            $data = [
                "ticket" => $TICKET,
                "service" => "https://qiwi.com/j_spring_cas_security_check",
            ];


            $res = $curl->post('https://auth.qiwi.com/cas/sts', json_encode($data));

            $TICKET = $res->entity->ticket;

            $data = [
                "ticket" => $TICKET,
            ];

            $res = $curl->get('https://qiwi.com/j_spring_cas_security_check?ticket='.$TICKET);


            //перевод

            $curl->post('https://qiwi.com/person/state.action');
            $curl->get('https://qiwi.com/transfer.action');
            $curl->get('https://qiwi.com/transfer/form.action');

            $sum_frac = intval(($sum - intval($sum)) * 100);
            $sum_frac = $sum_frac < 10 ? "0".$sum_frac : $sum_frac;

            $data = [
                "extra['account']" => $to,
                "source" => "qiwi_RUB",
                "amountInteger" => intval($sum),
                "amountFraction" => $sum_frac,
                "currency" => "RUB",
                "extra['comment']" => $comment,
                "state" => "CONFIRM",
                "protected" => "true",
            ];



            $curl->setHeader('Accept', 'application/json');
            $curl->setHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
            $res = $curl->post("https://qiwi.com/user/payment/form/state.action?".http_build_query($data));

            $data['token'] = $res->data->token;


            $curl->post("https://qiwi.com/user/payment/form/state.action?".http_build_query($data));



            $res = $curl->post("https://qiwi.com/user/payment/form/state.action?state=PAY");


            $res = $curl->post("https://qiwi.com/user/payment/form/state.action?state=PAY&token=".$res->data->token);

            $html = str_get_html($res);

            if($html->find("h2", 0)->plaintext == "Успешно")
                return true;
            else
                return false;
}
