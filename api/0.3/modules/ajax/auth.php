<?php
    session_start();
    include_once(__DIR__."/../../conf.php");

    if (!is_ajax()) {
        throw_json_error("Неверный запрос!");
    }

    if (post('method') === 'auth_stage_1' ) {
        if (!is_phone(trim(post('phone')))) {
            throw_json_error("Не указан номер телефона, либо он неверен!");
        }
        $phone = preg_replace('#[^0-9]+#','', post('phone'));
        $code = generate_code(5);
        $user = get_row_by_field('users', "phone", $phone);
        if (!$user) {
            $sql = "INSERT INTO users SET login = ".escape_db($phone).", phone = ".escape_db($phone).", date = ".escape_db(time());
            $sql .= ", code = ".escape_db($code);
            $sql .= ", code_expiration_date = ".escape_db(time() + CODE_EXPIRATION_DATE);
            $mysqli->query($sql);
        } else {
            if ($user['code_expiration_date'] > time()) {
                throw_json_error("Повторная отправка кода возможна только через ".(CODE_EXPIRATION_DATE / 60 )." мин.");
            }
            $sql = "UPDATE users SET code = ".escape_db($code);
            $sql .= ", code_expiration_date = ".escape_db(time() + CODE_EXPIRATION_DATE);
            $sql .= " WHERE phone = ".escape_db($phone);
            $mysqli->query($sql);
        }
        //взводим сессию
        $_SESSION['auth_phone'] = $phone;
        $mes = "На телефон {$phone} выслан секретный код: {$code}";
        send_json_message($mes);
    } else if (post('method') === 'auth_stage_2') {
        if (!post('code')) {
            throw_json_error("Код не верен!");
        }
        $phone = preg_replace('#[^0-9]+#','', session('auth_phone'));
        $sql = "SELECT COUNT(id) FROM users WHERE phone = ".escape_db(session('auth_phone'))." AND code = ".escape_db(post('code'));
        if (!get_value($sql)) {
            throw_json_error("Секретный код указан не верно!");
        }
        //авторизую юзера
        login();
        $mes = "Вы успешно вошли под номером {$phone}!";
        $data = make_data(["location" => "index.php?page=profile"], $mes);
        send_json($data);
    } else {
        throw_json_error("Ошибка метода запроса!");
    }
