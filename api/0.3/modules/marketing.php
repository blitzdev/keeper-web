<?php

    function marketing_login_balance($login, $currency="usd", $system=false) {
        static $cache = [];
        if (isset($cache[$login][$currency][$system])) {
            return $cache[$login][$currency][$system];
        }
        global $mysqli;
        $summa = 0;
        $summa += marketing_login_invest($login, $currency, $system);
        $summa -= marketing_login_payment($login, $currency, $system);
        $summa -= marketing_login_depozit($login, $currency, $system);
        $summa += marketing_login_profit($login, $currency, $system);
        $summa += marketing_login_referal($login, $currency, $system);
        return $cache[$login][$currency][$system] = $summa;
    }


    function marketing_login_invest($login, $currency="usd", $system=false) {
        static $cache = [];
        if (isset($cache[$login][$currency][$system])) {
            return $cache[$login][$currency][$system];
        }
        global $mysqli;
        $currency_type = get_row_by_target('currency_type', $currency);
        if (!$currency_type || !(bool)$currency_type['status']) {
            return $cache[$login][$currency][$system] = 0;
        }
        $sql = "SELECT SUM(summa) FROM `invest` WHERE login = ".escape_db($login)." AND status = ".escape_db(1)." AND `currency_type` = ".escape_db($currency_type['name']);
        if ($system) {
            $pay_system = get_row_by_target('pay_systems', $system);
            if (!$pay_system || !(bool)$pay_system['status']) {
                return $cache[$login][$currency][$system] = 0;
            }
            $sql .= " AND `pay_system` = ".escape_db($pay_system['name']);
        }
        return $cache[$login][$currency][$system] = get_value($sql);
    }


    function marketing_login_payment($login, $currency="usd", $system=false) {
        static $cache = [];
        if (isset($cache[$login][$currency][$system])) {
            return $cache[$login][$currency][$system];
        }
        global $mysqli;
        $currency_type = get_row_by_target('currency_type', $currency);
        if (!$currency_type || !(bool)$currency_type['status']) {
            return $cache[$login][$currency][$system] = 0;
        }
        $sql = "SELECT SUM(summa) FROM `payment` WHERE login = ".escape_db($login)." AND `currency_type` = ".escape_db($currency_type['name']);
        if ($system) {
            $pay_system = get_row_by_target('pay_systems', $system);
            if (!$pay_system || !(bool)$pay_system['status']) {
                return $cache[$login][$currency][$system] = 0;
            }
            $sql .= " AND `pay_system` = ".escape_db($pay_system['name']);
        }

        return $cache[$login][$currency][$system] = get_value($sql);
    }


    function marketing_login_depozit($login, $currency="usd", $system=false) {
        static $cache = [];
        if (isset($cache[$login][$currency][$system])) {
            return $cache[$login][$currency][$system];
        }
        global $mysqli;
        $currency_type = get_row_by_target('currency_type', $currency);
        if (!$currency_type || !(bool)$currency_type['status']) {
            return $cache[$login][$currency][$system] = 0;
        }
        $sql = "SELECT SUM(summa) FROM `depozit` WHERE login = ".escape_db($login)." AND status = ".escape_db(0)." AND `currency_type` = ".escape_db($currency_type['name']);
        if ($system) {
            $pay_system = get_row_by_target('pay_systems', $system);
            if (!$pay_system || !(bool)$pay_system['status']) {
                return $cache[$login][$currency][$system] = 0;
            }
            $sql .= " AND `pay_system` = ".escape_db($pay_system['name']);
        }
        return $cache[$login][$currency][$system] = get_value($sql);
    }


    //реферальные для участника
    function marketing_login_referal($login, $currency="usd", $system=false) {
        static $cache = [];
        if (isset($cache[$login][$currency][$system])) {
            return $cache[$login][$currency][$system];
        }
        $sum = 0;
        $row = get_list('referal_percent', "status = 1");
        for ($i = 0; $i < count($row); $i += 1) {
            $sum += marketing_login_refer_by_level($login, $i + 1, $currency, $system);
        }
        return $cache[$login][$currency][$system] = $sum;
    }


    function marketing_login_profit($login, $currency="usd", $system=false) {
        static $cache = [];
        if (isset($cache[$login][$currency][$system])) {
            return $cache[$login][$currency][$system];
        }
        global $mysqli;
        $currency_type = get_row_by_target('currency_type', $currency);
        if (!$currency_type || !(bool)$currency_type['status']) {
            return $cache[$login][$currency][$system] = 0;
        }
        $sql = "SELECT SUM(summa) FROM `profit` WHERE login = ".escape_db($login)." AND status = ".escape_db(1)." AND `currency_type` = ".escape_db($currency_type['name']);
        if ($system) {
            $pay_system = get_row_by_target('pay_systems', $system);
            if (!$pay_system || !(bool)$pay_system['status']) {
                return $cache[$login][$currency][$system] = 0;
            }
            $sql .= " AND `pay_system` = ".escape_db($pay_system['name']);
        }
        return $cache[$login][$currency][$system] = get_value($sql);
    }


    function marketing_system_balance($currency="usd", $system=false) {
        static $cache = [];
        if (isset($cache[$currency][$system])) {
            return $cache[$currency][$system];
        }
        global $mysqli, $percent_admin;
        $summa = 0;

        $balans_invest = marketing_system_invest($currency, $system);
        $summa += $balans_invest;
        $summa -= marketing_system_payment($currency, $system);
        $summa -= $balans_invest / 100 * $percent_admin;

        return $cache[$currency][$system] = $summa;
    }


    function marketing_system_payment($currency="usd", $system=false) {
        static $cache = [];
        if (isset($cache[$currency][$system])) {
            return $cache[$currency][$system];
        }
        global $mysqli, $fake_payment;
        $currency_type = get_row_by_target('currency_type', $currency);
        if (!$currency_type || !(bool)$currency_type['status']) {
            return $cache[$currency][$system] = 0;
        }
        $sql = "SELECT SUM(summa) FROM `payment` WHERE status = ".escape_db(1)." AND `currency_type` = ".escape_db($currency_type['name']);
        if ($system) {
            $pay_system = get_row_by_target('pay_systems', $system);
            if (!$pay_system || !(bool)$pay_system['status']) {
                return $cache[$currency][$system] = 0;
            }
            $sql .= " AND `pay_system` = ".escape_db($pay_system['name']);
        }
        return $cache[$currency][$system] = get_value($sql) + $fake_payment;
    }


    function marketing_system_invest($currency="usd", $system=false) {
        static $cache = [];
        if (isset($cache[$currency][$system])) {
            return $cache[$currency][$system];
        }
        global $mysqli, $fake_invest;
        $currency_type = get_row_by_target('currency_type', $currency);
        if (!$currency_type || !(bool)$currency_type['status']) {
            return $cache[$currency][$system] = 0;
        }
        $sql = "SELECT SUM(summa) FROM `invest` WHERE status = ".escape_db(1)." AND `currency_type` = ".escape_db($currency_type['name']);
        if ($system) {
            $pay_system = get_row_by_target('pay_systems', $system);
            if (!$pay_system || !(bool)$pay_system['status']) {
                return $cache[$currency][$system] = 0;
            }
            $sql .= " AND `pay_system` = ".escape_db($pay_system['name']);
        }
        return $cache[$currency][$system] = get_value($sql) + $fake_invest;
    }



    function get_currency_title($name) {
        static $cache = [];
        if (isset($cache[$name])) {
            return $cache[$name];
        }
        $row = get_row_by_target('currency_type', $name);
        if (!$row) {
            return $cache[$name] = false;
        }
        return $cache[$name] = $row['title'];
    }

    function get_currency_non($name) {
        static $cache = [];
        if (isset($cache[$name])) {
            return $cache[$name];
        }
        $row = get_row_by_target('currency_type', $name);
        if (!$row) {
            return $cache[$name] = false;
        }
        return $cache[$name] = $row['non'];
    }

    function get_currency_gen($name) {
        static $cache = [];
        if (isset($cache[$name])) {
            return $cache[$name];
        }
        $row = get_row_by_target('currency_type', $name);
        if (!$row) {
            return $cache[$name] = false;
        }
        return $cache[$name] = $row['gen'];
    }

    function get_currency_prefix($name) {
        static $cache = [];
        if (isset($cache[$name])) {
            return $cache[$name];
        }
        $row = get_row_by_target('currency_type', $name);
        if (!$row) {
            return $cache[$name] = false;
        }
        return $cache[$name] = $row['prefix'];
    }

    function get_currency_suffix($name) {
        static $cache = [];
        if (isset($cache[$name])) {
            return $cache[$name];
        }
        $row = get_row_by_target('currency_type', $name);
        if (!$row) {
            return $cache[$name] = false;
        }
        return $cache[$name] = $row['suffix'];
    }


    function get_pay_system_title($name) {
        static $cache = [];
        if (isset($cache[$name])) {
            return $cache[$name];
        }
        $row = get_row_by_target('pay_systems', $name);
        if (!$row) {
            return $cache[$name] = false;
        }
        return $cache[$name] = $row['title'];
    }

    function marketing_login_count_refer($login) {
        static $cache = [];
        if (isset($cache[$login])) {
            return $cache[$login];
        }
        $count = 0;
        //$count += marketing_count_refer_first_level($login);
        //$count += marketing_count_refer_second_level($login);
        //$count += marketing_count_refer_thrid_level($login);
        $row = get_list('referal_percent', "status = 1");
        for ($i = 0; $i < count($row); $i += 1) {
            $count += marketing_login_count_refer_by_level($login, $i + 1);
        }

        return $cache[$login] = $count;
    }

    function marketing_login_count_refer_by_level($login, $level=1) {
        static $cache = [];
        if (isset($cache[$login][$level])) {
            return $cache[$login][$level];
        }
        $referal_percent = get_row_by_field("referal_percent", "level", $level);
        if (!$referal_percent || $referal_percent['status'] == 0) {
            return $cache[$login][$level] = 0;
        }
        global $count;
        $tmp_global_count = $count;
        $count = 0;
        if (!function_exists("marketing_login_count_refer_backtrack")) {
            function marketing_login_count_refer_backtrack($login, $level=1) {
                global $count;
                if ($level < 1) {
                    return 0;
                }
                $users = get_list('users', 'refer = '.escape_db($login));
                foreach ($users as $user) {
                    if ($level === 1) {
                        $count += 1;
                    } else {
                        marketing_login_count_refer_backtrack($user['login'], $level - 1);
                    }
                }
                return $count;
            }
        }
        $count = marketing_login_count_refer_backtrack($login, $level);
        $tmp = $count;
        $count = $tmp_global_count;
        return $cache[$login][$level] = $tmp;
    }

    //получить количество рефералов 1 уровня
    function marketing_login_count_refer_first_level($login) {
        $referal_percent = get_row_by_field("referal_percent", "level", 1);
        if (!$referal_percent || $referal_percent['status'] == 0) {
            return 0;
        }
        global $mysqli;
        $sql = "SELECT COUNT(id) FROM `users` WHERE refer = ".escape_db($login).";";
        return get_value($sql);
    }

    ///получить количество рефералов 2 уровня
    function marketing_login_count_refer_second_level($login) {
        $referal_percent = get_row_by_field("referal_percent", "level", 2);
        if (!$referal_percent || $referal_percent['status'] == 0) {
            return 0;
        }
        global $mysqli;
        $count = 0;
        $users = get_list('users', 'refer = '.escape_db($login));
        foreach ($users as $user) {
            $sql = "SELECT COUNT(id) FROM `users` WHERE refer = ".escape_db($user['login']).";";
            $count += get_value($sql);
        }
        return $count;
    }

    //получить количество рефералов 3 уровня
    function marketing_login_count_refer_thrid_level($login) {
        $referal_percent = get_row_by_field("referal_percent", "level", 3);
        if (!$referal_percent || $referal_percent['status'] == 0) {
            return 0;
        }
        global $mysqli;
        $count = 0;
        $users = get_list('users', 'refer = '.escape_db($login));
        $common_list = [];
        foreach ($users as $user) {
            $list = get_list('users', 'refer = '.escape_db($user['login']));
            foreach ($list as $u) {
                $sql = "SELECT COUNT(id) FROM `users` WHERE refer = ".escape_db($u['login']).";";
                $count += get_value($sql);
            }
        }
        return $count;
    }


    function marketing_login_refer_by_level($login, $level=1, $currency="usd", $system=false, $status=1) {
        static $cache = [];
        if (isset($cache[$login][$level][$currency][$system][$status])) {
            return $cache[$login][$level][$currency][$system][$status];
        }
        global $sum;
        $tmp_global_sum = $sum;
        $sum = 0;
        if (!function_exists("marketing_login_refer_by_level_backtrack")) {
            function marketing_login_refer_by_level_backtrack($login, $level=1, $currency="usd", $system=false, $status=1, $require_level=1) {
                global $sum;
                if ($level < 1) {
                    return 0;
                }
                $users = get_list('users', 'refer = '.escape_db($login));
                foreach ($users as $user) {
                    if ($level === 1) {
                        $referal_percent = get_row_by_field("referal_percent", "level", $require_level);
                        if ($referal_percent && $referal_percent['status'] == 1) {
                            $where = "";
                            if ($currency) {
                                $currency_type = get_row_by_target('currency_type', $currency);
                                if (!$currency_type || !(bool)$currency_type['status']) {
                                    continue;
                                }
                                $where = " AND `currency_type` =".escape_db($currency_type['name']);
                            }
                            if ($system) {
                                $pay_system = get_row_by_target('pay_systems', $system);
                                if (!$pay_system || !(bool)$pay_system['status']) {
                                    continue;
                                }
                                $where .= " AND `pay_system` =".escape_db($pay_system['name']);
                            }
                            $sql = "SELECT SUM(summa) FROM `profit` WHERE login = ".escape_db($user['login']).$where." AND status = ".escape_db($status).";";
                            $sum += get_value($sql) / 100.0 * $referal_percent['percent'];
                        }
                    } else {
                        marketing_login_refer_by_level_backtrack($user['login'], $level - 1, $currency, $system, $status, $require_level);
                    }
                }
                return $sum;
            }
        }
        $sum = marketing_login_refer_by_level_backtrack($login, $level, $currency, $system, $status, $level);
        $tmp = $sum;
        $sum = $tmp_global_sum;
        return $cache[$login][$level][$currency][$system][$status] = $tmp;
    }

        //получить сумму с инвестиций рефералов 1 уровня
    function marketing_login_sum_refer_first_level($login, $currency=false, $system=false, $status=1) {
        $referal_percent = get_row_by_field("referal_percent", "level", 1);
        if (!$referal_percent || $referal_percent['status'] == 0) {
            return 0;
        }
        global $mysqli;
        $where = "";
        if ($currency) {
            $currency_type = get_row_by_target('currency_type', $currency);
            if (!$currency_type || !(bool)$currency_type['status']) {
                return 0;
            }
            $where = " AND `currency_type` =".escape_db($currency_type['name']);
        }
        if ($system) {
            $pay_system = get_row_by_target('pay_systems', $system);
            if (!$pay_system || !(bool)$pay_system['status']) {
                return 0;
            }
            $where .= " AND `pay_system` =".escape_db($pay_system['name']);
        }
        $sql = "SELECT SUM(summa) FROM `invest` WHERE refer = ".escape_db($login).$where." AND status = ".escape_db($status).";";
        return get_value($sql) / 100.0 * $referal_percent['percent'];
    }

    ///получить сумму с инвестиций рефералов 2 уровня
    function marketing_login_sum_refer_second_level($login, $currency=false, $system=false, $status=1) {
        $referal_percent = get_row_by_field("referal_percent", "level", 2);
        if (!$referal_percent || $referal_percent['status'] == 0) {
            return 0;
        }
        global $mysqli;
        $where = "";
        if ($currency) {
            $currency_type = get_row_by_target('currency_type', $currency);
            if (!$currency_type || !(bool)$currency_type['status']) {
                return 0;
            }
            $where = " AND `currency_type` =".escape_db($currency_type['name']);
        }
        if ($system) {
            $pay_system = get_row_by_target('pay_systems', $system);
            if (!$pay_system || !(bool)$pay_system['status']) {
                return 0;
            }
            $where .= " AND `pay_system` =".escape_db($pay_system['name']);
        }
        $sum = 0;
        $users = get_list('users', 'refer = '.escape_db($login));
        foreach ($users as $user) {
            $sql = "SELECT SUM(summa) FROM `invest` WHERE refer = ".escape_db($user['login']).$where." AND status = ".escape_db($status).";";
            $sum += get_value($sql);
        }
        return $sum / 100.0 * $referal_percent['percent'];
    }

    //получить сумму с инвестиций рефералов 3 уровня
    function marketing_login_sum_refer_thrid_level($login, $currency=false, $system=false, $status=1) {
        $referal_percent = get_row_by_field("referal_percent", "level", 3);
        if (!$referal_percent || $referal_percent['status'] == 0) {
            return 0;
        }
        global $mysqli;
        $where = "";
        if ($currency) {
            $currency_type = get_row_by_target('currency_type', $currency);
            if (!$currency_type || !(bool)$currency_type['status']) {
                return 0;
            }
            $where = " AND `currency_type` =".escape_db($currency_type['name']);
        }
        if ($system) {
            $pay_system = get_row_by_target('pay_systems', $system);
            if (!$pay_system || !(bool)$pay_system['status']) {
                return 0;
            }
            $where .= " AND `pay_system` =".escape_db($pay_system['name']);
        }
        $sum = 0;
        $users = get_list('users', 'refer = '.escape_db($login));
        $common_list = [];
        foreach ($users as $user) {
            $list = get_list('users', 'refer = '.escape_db($user['login']));
            foreach ($list as $u) {
                $sql = "SELECT SUM(summa) FROM `invest` WHERE refer = ".escape_db($u['login']).$where." AND status = ".escape_db($status).";";
                $sum += get_value($sql);
            }
        }
        return $sum / 100.0 * $referal_percent['percent'];
    }


    function marketing_get_plans($currency=false) {
        static $cache = [];
        if (isset($cache[$currency])) {
            return $cache[$currency];
        }
        $where = "";
        if ($currency) {
            $currency_type = get_row_by_target('currency_type', $currency);
            if (!$currency_type || !(bool)$currency_type['status']) {
                return $cache[$currency] = [];
            }
            $where = " AND `currency_type` =".escape_db($currency_type['name']);
        }
        $list = get_list('plans', "`status` = ".escape_db(1).$where);
        return $cache[$currency] = $list;
    }


    function marketing_get_plans_by_sum($sum) {
        static $cache = [];
        if (isset($cache[$sum])) {
            return $cache[$sum];
        }
        $list = marketing_get_plans();
        foreach ($list as $plan) {
            if ($plan['min'] <= $sum && $sum <= $plan['max']) {
                return $cache[$sum] = $plan;
            }
        }
        return $cache[$sum] = false;
    }

    function marketing_get_plans_by_id($id) {
        static $cache = [];
        if (isset($cache[$id])) {
            return $cache[$id];
        }
        $list = marketing_get_plans();
        foreach ($list as $plan) {
            if ((int)$plan['id']  === (int)$id) {
                return $cache[$id] = $plan;
            }
        }
        return $cache[$id] = false;
    }



    function get_plan_title($id) {
        static $cache = [];
        if (isset($cache[$id])) {
            return $cache[$id];
        }
        $row = get_row_by_id('plans', $id);
        if (!$row || !$row['status']) {
            return $cache[$id] = false;
        }
        return $cache[$id] = $row['title'];
    }

    //три параметра нужны чтобы проверить все на корректность
    function marketing_add_depozit($login, $summa, $currency, $invest_id) {
        global $mysqli;
        //получим
        $res = [
            "error" => true,
            "message" => "",
        ];

        $ip = ip_get();

        $invest = get_row_by_id('invest', $invest_id);
        if (!$invest) {
            $res['message'] = "Нет пополнения с идентификатором ".(int)$invest_id;
            return $res;
        }
        if ($invest['login'] != $login) {
            $res['message'] = "Логин ".escape($login)." не соответсвует, логину сохраненому в системе!";
            return $res;
        }
        /*if ((int)$invest['status']) {
            $res['message'] = "Платеж №".(int)$invest_id." уже активирован!";
            return $res;
        }*/

        $currency_type = get_row_by_target('currency_type', $currency);
        if (!$currency_type || !(bool)$currency_type['status']) {
            $res['message'] = "Некорректная валюта ".escape($currency);
            return $res;
        }
        if ($currency_type['name'] !== $invest['currency_type']) {
            $res['message'] = "Валюта открытия депозита не совпадает с валютой пополения №".(int)$invest_id."!";
            return $res;
        }
        //сравниваем как btc так как это даст наиболее точный результат
        if (format_btc($invest['summa']) !== format_btc($summa)) {
            $res['message'] = "Сумма открытия депозита не совпадает с суммой пополения №".(int)$invest_id."!";
            return $res;
        }
        $plan = marketing_get_plans_by_id($invest['plan_id']);
        if (!$plan) {
            $res['message'] = "Не удалось извлечь план №".(int)$invest_id."!";
            return $res;
        }

        $sql = "INSERT INTO `depozit` SET ";
        $sql .= "login = ".escape_db($login);
        $sql .= ", summa = ".escape_db($summa);
        $sql .= ", summa_plus = ".escape_db($summa + ($summa / 100 * $plan['percent'] * $plan['count']));
        $sql .= ", date_start = ".escape_db($invest['date']);
        $sql .= ", date_end = ".escape_db($invest['date'] + $plan['seconds'] * $plan['count']);
        $sql .= ", status = ".escape_db(0);
        $sql .= ", plan_id = ".escape_db($plan['id']);
        $sql .= ", currency_type = ".escape_db($currency_type['name']);
        $sql .= ", pay_system = ".escape_db($invest['pay_system']);
        $sql .= ", invest_id = ".escape_db($invest['id']);
        $sql .= ", ip = ".escape_db($ip);
        $mysqli->query($sql);

        $depozit_id = $mysqli->insert_id;

        for ($i = 1; $i <= $plan['count']; $i += 1) {
            $sql = "INSERT INTO `profit` SET ";
            $sql .= "login = ".escape_db($login);
            $sql .= ", depozit_id = ".escape_db($depozit_id);
            $sql .= ", summa = ".escape_db($summa / 100 * $plan['percent']);
            $sql .= ", date_end = ".escape_db($invest['date'] + $plan['seconds'] * $i);
            $sql .= ", status = ".escape_db(0);
            $sql .= ", currency_type = ".escape_db($currency_type['name']);
            $sql .= ", pay_system = ".escape_db($invest['pay_system']);
            $sql .= ", invest_id = ".escape_db($invest['id']);
            //$sql .= ", ip = ".escape_db($ip);
            $mysqli->query($sql);
        }
        $res['error'] = false;
        $res['message'] = "Депозит успешно создан!";
        return $res;
    }

    //проверить поддержку валюты платежной системой
    function check_currency_of_pay_system($currency, $pay_system) {
        static $cache = [];
        if (isset($cache[$currency][$pay_system['id']])) {
            return $cache[$currency][$pay_system['id']];
        }
        if (!isset($pay_system['target'])) {
            return $cache[$currency][$pay_system['id']] = false;
        }
        return $cache[$currency][$pay_system['id']] = (strpos($pay_system['target'], $currency) !== false);
    }


    //создание депозита по плану три параметра нужны чтобы проверить все на корректность
    function marketing_add_depozit_by_plan($login, $summa, $currency, $pay_system, $plan_id, $transfer="") {
        global $mysqli;
        $res = [
            "error" => true,
            "message" => "",
        ];
        $user = get_row_by_field('users', 'login', $login);
        if (!$user) {
            $res['message'] = "Пользователь не зарегестрирован в системе!";
            return $res;
        }
        $currency_type = get_row_by_target('currency_type', $currency);
        if (!$currency_type || !(bool)$currency_type['status']) {
            $res['message'] = "Некорректная валюта ".escape($currency);
            return $res;
        }
        $pay_system = get_row_by_target('pay_systems', $pay_system);
        if (!$pay_system || !(bool)$pay_system['status']) {
            $res['message'] = "Некорректная платежная система ".escape($pay_system);
            return $res;
        }
        if (!check_currency_of_pay_system($currency_type['name'], $pay_system)) {
            $res['message'] = "Эта валюта не поддерживается платежной системой!";
            return $res;
        }
        $plan = marketing_get_plans_by_id($plan_id);
        if (!$plan) {
            $res['message'] = "Не удалось извлечь план №".(int)$plan_id."!";
            return $res;
        }
        if ($plan['currency_type'] !== $currency_type['name']) {
            $res['message'] = "Эта валюта не поддерживается тарифным планом!";
            return $res;
        }
        if ((float)$summa < $plan['min'] || (float)$summa > $plan['max']) {
            $res['message'] = "Вы ввели некорректную сумму!";
            return $res;
        }
        //новинка
        //$mysqli->begin_transaction(MYSQLI_TRANS_START_READ_ONLY);
        $sql = "INSERT INTO `invest` (login, summa, date, refer, pay_system, plan_id, currency_type, status, transfer) VALUES (".escape_db($login).", ".escape_db($summa).", ".escape_db(time()).", ".escape_db($user['refer']).", ".escape_db($pay_system['name']).", ".escape_db($plan_id).", ".escape_db($currency_type['name']).", ".escape_db(1).", ".escape_db($transfer).")";
        $result = $mysqli->query($sql);
        if (!$result) {
            $res['message'] = "Ошибка записи предплатежа!";
            return $res;
        }
        $invest_id = $mysqli->insert_id;

        $res = marketing_add_depozit($login, $summa, $currency, $invest_id);
        if ($res['error']) {
            $sql = "DELETE FROM invest WHERE id = ".escape_db($invest_id);
            $mysqli->query($sql);
        } else {
        }
        return $res;
    }


    //добавить депозит по плану с баланса
    function marketing_add_depozit_by_plan_from_balance($login, $summa, $currency, $plan_id) {
        global $mysqli;

        $ip = ip_get();
        //получим
        $res = [
            "error" => true,
            "message" => "",
        ];

        /*if ((int)$invest['status']) {
            $res['message'] = "Платеж №".(int)$invest_id." уже активирован!";
            return $res;
        }*/
        $currency_type = get_row_by_target('currency_type', $currency);
        if (!$currency_type || !(bool)$currency_type['status']) {
            $res['message'] = "Некорректная валюта ".escape($currency);
            return $res;
        }
        $plan = marketing_get_plans_by_id($plan_id);
        if (!$plan) {
            $res['message'] = "Не удалось извлечь план №".(int)$plan_id."!";
            return $res;
        }

        if ($plan['currency_type'] !== $currency_type['name']) {
            $res['message'] = "Эта валюта не поддерживается тарифным планом!";
            return $res;
        }
        if ((float)$summa < $plan['min'] || (float)$summa > $plan['max']) {
            $res['message'] = "Вы ввели некорректную сумму!";
            return $res;
        }


        $data_start = time();
        $sql = "INSERT INTO `depozit` SET ";
        $sql .= "login = ".escape_db($login);
        $sql .= ", summa = ".escape_db($summa);
        $sql .= ", summa_plus = ".escape_db($summa + ($summa / 100 * $plan['percent'] * $plan['count']));
        $sql .= ", date_start = ".escape_db($data_start);
        $sql .= ", date_end = ".escape_db($data_start + $plan['seconds'] * $plan['count']);
        $sql .= ", status = ".escape_db(0);
        $sql .= ", plan_id = ".escape_db($plan['id']);
        $sql .= ", currency_type = ".escape_db($currency_type['name']);
        $sql .= ", pay_system = ".escape_db("balance");
        $sql .= ", ip = ".escape_db($ip);
        $mysqli->query($sql);

        $depozit_id = $mysqli->insert_id;

        for ($i = 1; $i <= $plan['count']; $i += 1) {
            $sql = "INSERT INTO `profit` SET ";
            $sql .= "login = ".escape_db($login);
            $sql .= ", depozit_id = ".escape_db($depozit_id);
            $sql .= ", summa = ".escape_db($summa / 100 * $plan['percent']);
            $sql .= ", date_end = ".escape_db($data_start + $plan['seconds'] * $i);
            $sql .= ", status = ".escape_db(0);
            $sql .= ", currency_type = ".escape_db($currency_type['name']);
            $sql .= ", pay_system = ".escape_db("balance");
            //$sql .= ", ip = ".escape_db($ip);
            $mysqli->query($sql);
        }
        $res['error'] = false;
        $res['message'] = "Депозит успешно создан!";
        return $res;
    }


    //старший акционер
    function marketing_senior_shareholder() {
        global $admin_login, $free_referal;
        static $user = [];

        if (!$user) {
            if (session('refer') && session('refer') != $admin_login && session('refer') != $free_referal) {
                $user = get_row_by_field('users', 'login', session('refer'));
                $user['img'] = !empty($user['img']) ? $user['img'] : "assets/profile/img/anon.png";
                return $user;
            }
            $list = get_list('users');
            $user = [];
            $count = 0;
            foreach ($list as $row) {
                $tmp_count = marketing_login_count_refer($row['login']);
                if ($tmp_count > $count) {
                    $count = $tmp_count;
                    $user = $row;
                }
            }
            $user['img'] = !empty($user['img']) ? $user['img'] : "assets/profile/img/anon.png";
        }

        return $user;
    }


    //получить рефералов n уровня для $login
    function marketing_login_get_refer_by_level($login, $level=1) {
        $referal_percent = get_row_by_field("referal_percent", "level", $level);
        if (!$referal_percent || $referal_percent['status'] == 0) {
            return 0;
        }
        global $refer;
        $tmp_global_refer = $refer;
        $refer = [];
        if (!function_exists("marketing_login_get_refer_backtrack")) {
            function marketing_login_get_refer_backtrack($login, $level=1) {
                global $refer;
                if ($level < 1) {
                    return 0;
                }
                $users = get_list('users', 'refer = '.escape_db($login));
                foreach ($users as $user) {
                    if ($level === 1) {
                        $refer[] = $user;
                    } else {
                        marketing_login_get_refer_backtrack($user['login'], $level - 1);
                    }
                }
                return $refer;
            }
        }
        $refer = marketing_login_get_refer_backtrack($login, $level);
        $tmp = $refer;
        $refer = $tmp_global_refer;
        return $tmp;
    }


    //получить количество ячеек для покупки
    function marketing_plan_max_count_used($currency=false, $plan_id=false) {
        $where = "";
        if ($currency) {
            $currency_type = get_row_by_target('currency_type', $currency);
            if (!$currency_type || !(bool)$currency_type['status']) {
                return 0;
            }
            $where = " AND `currency_type` =".escape_db($currency_type['name']);
        }
        if ($plan_id) {
            $plan = marketing_get_plans_by_id($plan_id);
            if (!$plan) {
                return 0;
            }
            $where = " AND `id` =".escape_db($plan['id']);
        }
        $sql = "SELECT SUM(max_count_used) FROM plans WHERE 1 = 1 ".$where;
        return (float)get_value($sql);
    }

    //получить количество купленных ячеек(всеми пользователями)
    function marketing_count_invest($currency=false, $plan_id=false, $status=1) {
        $where = "";
        if ($currency) {
            $currency_type = get_row_by_target('currency_type', $currency);
            if (!$currency_type || !(bool)$currency_type['status']) {
                return 0;
            }
            $where = " AND `currency_type` =".escape_db($currency_type['name']);
        }
        if ($plan_id) {
            $plan = marketing_get_plans_by_id($plan_id);
            if (!$plan) {
                return 0;
            }
            $where = " AND `plan_id` =".escape_db($plan['id']);
        }
        $sql = "SELECT COUNT(id) FROM invest WHERE `status` = ".escape_db((int)$status).$where;
        return (int)get_value($sql);
    }

    //получить количество купленных ячеек одним пользователем
    function marketing_login_count_invest($login, $currency=false, $plan_id=false, $status=1) {
        $where = "";
        if ($currency) {
            $currency_type = get_row_by_target('currency_type', $currency);
            if (!$currency_type || !(bool)$currency_type['status']) {
                return 0;
            }
            $where = " AND `currency_type` =".escape_db($currency_type['name']);
        }
        if ($plan_id) {
            $plan = marketing_get_plans_by_id($plan_id);
            if (!$plan) {
                return 0;
            }
            $where = " AND `plan_id` =".escape_db($plan['id']);
        }
        $sql = "SELECT COUNT(id) FROM invest WHERE `login` = ".escape_db($login)." AND `status` = ".escape_db((int)$status).$where;
        return (int)get_value($sql);
    }
