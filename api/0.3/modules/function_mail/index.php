<?php

    //to admin должен быть
    function mail_send_to_support($to, $name, $email, $subject, $message) {
        $data = [
            "error" => true,
            "message" => "Сообщение отрпавлено!",];
        $mail = new Mail($to);
        
        if (empty($name)) {
            $data["message"] = "Вы не ввели имя!";
            return $data;
        }
        
        if (empty($email) || !filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $data["message"] = "Вы ввели неправильный E-mail!";
            return $data;
        }

        if (empty($message)) {
            $data["message"] = "Вы не ввели текст сообщения!";
            return $data;
        }

        //собираем письмо с использованием буфферов
        ob_start();
        include(__DIR__."/tags/send_to_support.php");
        $text = ob_get_clean();

        
        if ($mail->send($to, $subject, $text)) {
            $data = [
                "error" => false,
                "message" => "Сообщение отрпавлено!",];
        } else {
            $data["message"] = "Ошибка отправки!";
        }
        return $data;
    }