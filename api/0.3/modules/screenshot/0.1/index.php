<?php
    /* Модуль требует окружение модулей translate, function_update, conf.php*/

    /*Оригинальные ссылки приложения*/
    define("ORIGIN_RELATIVE_PATH_PAYEER", "screenshot_service/origin/payeer/1C01E848E4807711A0D98718BBD7E54A9B/");
    define("ORIGIN_RELATIVE_PATH_QIWI", "screenshot_service/origin/qiwi/AC02B90CA65ADE284A5504E43A2A43/");
    define("ORIGIN_PATH_PAYEER", ROOT."/".ORIGIN_RELATIVE_PATH_PAYEER);
    define("ORIGIN_PATH_QIWI", ROOT."/".ORIGIN_RELATIVE_PATH_QIWI);

    //дирректория для сохранения пользовательских ссылок
    define("ORDER_RELATIVE_PATH", "screenshot_service/orders/");
    define("ORDER_PATH", ROOT."/".ORDER_RELATIVE_PATH);
    //сгенерировать новую ссылку
    function screenshot_generate_link($system) {
        $salt = "UJLHJ:LTYBZ{HBCNF}";
        $link = hash("sha256", uniqid().$salt.microtime());
        $link = ORDER_RELATIVE_PATH.$system."/".$link;
        return $link;
    }
    //создать новый заказ
    function screenshot_create_order($system, $user=[], $invest=[]) {
        global $mysqli;
        $res = [];
        $date_start = time();
        $date_end = $date_start + LINK_LIFE_TIME;
        $link = screenshot_generate_link($system);
        $res = false;
        if ($system == "payeer") {
            $res = symlink(ORIGIN_PATH_PAYEER, ROOT."/".$link);
        }
        if ($system == "qiwi") {
            $res = symlink(ORIGIN_PATH_QIWI, ROOT."/".  $link);
        }
        if (!$res) {
            $res = message_error(T("Ошибка формирования временной ссылки!"));
			return $res;
        }
        $sql = "INSERT INTO orders SET ";
        $sql .= "  system = ".escape_db($system);
        $sql .= ", date_start = ".escape_db($date_start);
        $sql .= ", date_end = ".escape_db($date_end);
        $sql .= ", link = ".escape_db($link);
        $sql .= ", summa = ".escape_db($invest["summa"]);
        $sql .= ", transaction = ".escape_db($invest["transaction"]);
        $sql .= ", wallet = ".escape_db($invest["wallet"]);
        $sql .= ", ip = ".escape_db($user["ip"]);
        $sql .= ", comment = ".escape_db("Создание услуги ScreenShot за ".PRICE_SERVICE."руб. для ".$system);
        $sql .= ", status = ".escape_db(1);
        if ($mysqli->query($sql)) {
            $res = message_success(T("Уникальная ссылка для доступа к сервису успешно сгенерирована!"));
            $res["data"]["link"] = request_get_protocol().server("SERVER_NAME")."/".$link;
            $res["data"]["date_end"] = $date_end;
        } else {
            $res = message_error(T("Ошибка генерации ссылки!"));
            $res["data"]["link"] = "";
            $res["data"]["date_end"] = time();;
        }

        return $res;
    }

    //удалить все просроченные заказы
    function screenshot_delete_expiried() {
        $list = get_list("orders", "date_end < ".escape_db(time()));
        foreach ($list as $item) {
            @unlink(ROOT."/".$item["link"]);
        }
    }

    screenshot_delete_expiried();
