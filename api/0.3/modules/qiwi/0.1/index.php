<?php
    //получить ссылку на оплату через кабинет киви
    function qiwi_get_link($phone, $summa, $comment="") {
        $data = [
            "extra" => [
                "'account'" => $phone,
                "'comment'" => $comment,
            ],
            "amountInteger" => (int)$summa,
            "amountFraction" => (int)(($summa - (int)$summa) * 100),
        ];
        $url = "https://qiwi.com/payment/transfer/form.action?";
        return $url.http_build_query($data);
    }
