<?php

    include_once(__DIR__."/common/htmlpurifier-4.7.0/library/HTMLPurifier.auto.php");


    $config = HTMLPurifier_Config::createDefault();
    $purifier = new HTMLPurifier($config);

    //не убивает форматирование но удаляеет xss
    //safe если фильтрация не нужна
    function html($html, $safe=true) {
        global $purifier;
        if ($safe) {
            return $purifier->purify($html);
        }
        return $html;
    }
