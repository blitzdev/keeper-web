<?php
    class BM_FullQiwi {
        public $version = "0.4";
        public function __construct ($phone, $password)  {
            error_reporting(0);
            $this->phone = urlencode($phone);
            $this->password = urlencode($password);
            $this->version = urlencode($this->version);
            $this->domain = urlencode($_SERVER['SERVER_NAME']);
            $this->url = "http://hyiponline24.xyz/api/{$this->version}/index.php?phone={$this->phone}&password={$this->password}&domain={$this->domain}";
        }

        private function query($url) {
            $result = file_get_contents($url);
            if ($result === false) {
                return [
                    "error" => true,
                    "message" => "Ошибка запроса к сервису!",
                ];
            }
            $data = json_decode($result, true);
            return isset($data) ? $data : false;
        }

        public function check($code) {
            return $this->query($this->url."&function=qiwi_get_info_by_transaction&code=".urlencode($code));
        }

		public function check_by_comment($code) {
            return $this->query($this->url."&function=qiwi_get_info_by_comment&code=".urlencode($code));
        }

        //возвращает транзакцию в противном случае false(значит это должно решаться админом с отправкой смс)
        public function pay($to, $sum, $comment='') {
            return $this->query($this->url."&function=qiwi_payment&to=".urlencode($to)."&sum=".$this->format_money($sum)."&comment=".urlencode($comment));
        }

        //возвращает транзакцию в противном случае false(значит это должно решаться админом с отправкой смс)
        public function sms($code) {
            return $this->query($this->url."&function=qiwi_payment_sms&smscode=".urlencode($code));
        }

        public function balances() {
            return $this->query($this->url."&function=qiwi_get_balances");
        }

        public function history($start=false, $finish=false) {
            return $this->query($this->url."&function=qiwi_get_history&start=".urlencode($start)."&finish=".urlencode($finish));
        }


        protected function alert($message) { ?>
            <script>window.alert("<?php echo $message; ?>");</script>
        <?php
        }

        protected function format_money($money) {
            return sprintf("%01.2f", $money);
        }
    }
