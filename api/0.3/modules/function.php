<?php
    //проверка авторизации
    function auth_user(){
        global $mysqli;
        if(isset($_SESSION['login']) && isset($_SESSION['password'])){
            return true;
        } else {
            return false;
        }
    }
    
    //проверка Логина на валидность и занятость при регистрации
    function check_login_reg($login, $link){
        global $mysqli;
        $login = preg_replace('#[^a-zA-Z\-\_0-9]+#','', $login);
        $login = trim($login);
        
        //проверка логина на длинну
        if(strlen($login) < 3) {
            $_SESSION['message'] = 'Логин не меньше 3 символов!';
            header("Location: $link");
            exit;
        }
        
        //проверка логина на занятость
        $result_login = $mysqli->query("SELECT login FROM `users` WHERE login LIKE '$login'");
        if($result_login->num_rows > 0){ 
            $_SESSION['message'] = 'Этот логин занят!';
            header("Location: $link");
            exit;
        }
        
        return 0;
    }

    //проверка E-mail на валидность и занятость при регистрации
    function check_email($email, $link){
        global $mysqli;
        //если E-mail введен не корректно - возвращаем ошибку!
        if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
            $_SESSION['message'] = "Введите корректный E-mail";
            header("Location: $link");
            exit;
        }
        
        //проверка E-mail на занятость в базе данных
        $result = $mysqli->query("SELECT email FROM `users` WHERE email LIKE '$email'");
        if($result->num_rows > 0){ 
            $_SESSION['message'] = 'Этот E-mail занят!';
            header("Location: $link");
            exit;
        }
        
        return 0;
    }
    
    //проверка E-mail на валидность в кабинете
    function check_email_settings($email){
        global $mysqli;
        $text = "";
        //если E-mail введен не корректно - возвращаем ошибку!
        if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
            $text = "Неккоректно введен E-mail";
            return $text;
        }
        
        //проверка E-mail на занятость в базе данных
        $result = $mysqli->query("SELECT email FROM `users` WHERE email LIKE '$email'");
        if($result->num_rows > 0){ 
            $text = "Этот E-mail занят другим пользователем";
            return $text;
        }
        
        return 0;
    }
    
    //Сравнение E-mail -ов при регистрации
    function parity_email($email, $email1, $link){
        global $mysqli;
        if($email != $email1){
            $_SESSION['message'] = 'Указанные вами E-mail не совпадают';
            header("Location: $link");
            exit;
        }
    
        return 0;
    }
    
    //проверка пароля на валидность при регистрации
    function check_password($password, $link){
        global $mysqli;
        $password = preg_replace('#[^a-zA-Z\-\_0-9]+#','',$password);
        
        //проверяем пароль на длинну
        if(strlen($password) < 4) {
            $_SESSION['message'] = 'Пароль не менее 4 символов';
            header("Location: $link");
            exit;
        }
        
        return 0;
    }
    
    //Сравнение паролей при регистрации
    function parity_password($password, $password1, $link){
        if($password != $password1){
            $_SESSION['message'] = 'Пароли не совпадают';
            header("Location: $link");
            exit;
        }
    
        return 0;
    }
    
    //логин реферовода при регистрации, если нету ставим под свободного реферала!
    function referovod_reg($free_referal){
        $ref_login = '';
        if(!empty($_SESSION['ref_login'])) {
            $ref_login = $_SESSION['ref_login']; 
        } else {
            $ref_login = $free_referal;
        }
        
        return $ref_login;
    }
    
    //очистка данных
    function clear_data($str){
        $str = trim($str);
        $str = strip_tags($str);
        $str = htmlspecialchars($str);
        return $str;
    }
    
    // ip юзера
    function check_ip() {
        if (getenv('HTTP_CLIENT_IP') && strcasecmp(getenv('HTTP_CLIENT_IP'), 'unknown')) {
            $ip = getenv('HTTP_CLIENT_IP');
        } elseif (getenv('HTTP_X_FORWARDED_FOR') && strcasecmp(getenv('HTTP_X_FORWARDED_FOR'), 'unknown')) {
            $ip = getenv('HTTP_X_FORWARDED_FOR');
        } elseif (getenv('REMOTE_ADDR') && strcasecmp(getenv("REMOTE_ADDR"), 'unknown')) {
            $ip = getenv('REMOTE_ADDR');
        } elseif (!empty($_SERVER['REMOTE_ADDR']) && strcasecmp($_SERVER['REMOTE_ADDR'], 'unknown')) {
            $ip = $_SERVER['REMOTE_ADDR'];
        } else {
            $ip = 'unknown';
        }
        return $ip;
    }
    
    //считаем сколько онлайн юзеров
    function count_online($ip, $time) {
        global $mysqli;

        if ($ip != 'unknown') {
            $ip = preg_replace("#[^0-9]+#i", '', $ip);
            $last_time = $time + 20 * 60;
            $result = $mysqli->query("SELECT last_time FROM online WHERE ip = '$ip'");
            if ($result && $result->num_rows > 0) {
                $mysqli->query("UPDATE online SET last_time=$last_time WHERE ip = '$ip' LIMIT 1");
            } else {
                $mysqli->query("INSERT INTO online (ip,last_time) VALUES ('$ip',$last_time)");
            }
            $mysqli->query('DELETE FROM online WHERE last_time<' . $time);
        }
        $result = $mysqli->query('SELECT * FROM online');
        return $result ? $result->num_rows : 1;
    }

    //колличество юзеров в системе
    function count_user() {
        global $mysqli;

        $user = 0;
        $result = $mysqli->query("SELECT count(*) as `count` FROM `users`");
        if ($result && $row = $result->fetch_assoc()) {
            $user = $row['count'];
            $result->free();
        }
        return $user;
    }

    //сумма всех пополнений системы
    function balans_invest() {
        global $mysqli;
        
        $summa = 0;
        $result = $mysqli->query("SELECT SUM(summa) as `summa` FROM `invest` WHERE status = '1'");
        if ($result && $row = $result->fetch_assoc()) {
            $summa = $row['summa'];
            $result->free();
        }

        return $summa;
    }
    
    //сумма всех выплат системы
    function balans_payment() {
        global $mysqli;

        $summa = 0;
        $result = $mysqli->query("SELECT SUM(summa) as `summa` FROM `payment` WHERE status = '1'");
        if ($result && $row = $result->fetch_assoc()) {
            $summa = $row['summa'];
            $result->free();
        }
        
        return $summa;
    }
    
    //логин реферовода для реферала, тот кто приел участника
    function referovod_login($login) {
        global $mysqli;

        $ref = '';
        $result = $mysqli->query("SELECT refer FROM `users` WHERE login = '$login'");
        if($result && $row = $result->fetch_assoc()) {
            $ref = $row['refer']; //логин реферала
        }
        //реферовод
        return $ref;
    }
    
    //Дата регистрации участника
    function reg_date($login) {
        global $mysqli;

        $date = '';
        $result = $mysqli->query("SELECT date FROM `users` WHERE login = '$login'");
        if($result && $row = $result->fetch_assoc()) {
            $date = $row['date'];
        }
        return $date;
    }
    
    //сумма пополений для участника
    function login_invest($login) {
        global $mysqli;

        $summa = 0;
        $result = $mysqli->query("SELECT SUM(summa) as `summa` FROM `invest` WHERE login = '$login' AND status = '1'");
        if ($result && $row = $result->fetch_assoc()) {
            $summa = $row['summa'];
            $result->free();
        }
        
        return $summa;
    }
    
    //сумма выплат для участника
    function login_payment($login) {
        global $mysqli;

        $summa = 0;
        $result = $mysqli->query("SELECT SUM(summa) as `summa` FROM `payment` WHERE login = '$login'");
        if ($result && $row = $result->fetch_assoc()) {
            $summa = $row['summa'];
            $result->free();
        }

        return $summa;
    }
    
    //реферальные для участника
    function login_referal($login, $p_ref) {
        global $mysqli;

        $summa = 0;
        $result = $mysqli->query("SELECT SUM(summa) as `summa` FROM `invest` WHERE refer = '$login' AND status = '1'");
        if ($result && $row = $result->fetch_assoc()) {
            $summa = $row['summa'];
            $result->free();
        }

        $summa = $summa / 100 * $p_ref;

        return $summa;
    }
    
    //сумма вкладов которые в работе для участника
    function login_depozit($login) {
        global $mysqli;
        
        $summa = 0;
        
        $result = $mysqli->query("SELECT SUM(summa) as `summa` FROM `depozit` WHERE login = '$login'");
        if ($result && $row = $result->fetch_assoc()) {
            $summa = $row['summa'];
            $result->free();
        }
        
        return $summa;
    }
    
    //количество рефералов у участника
    function count_referal($login){
        global $mysqli;
        
        $count = 0;
        
        $result = $mysqli->query("SELECT count(*) as `count` FROM `users` WHERE refer = '$login'");
        if ($result && $row = $result->fetch_assoc()) {
            $count = $row['count'];
            $result->free();
        }
        return $count;
        
    }
    
    //профит с вкладов для участника
    function login_profit($login) {
        global $mysqli;
        
        $summa_profit = 0;
        
        $result_summa = $mysqli->query("SELECT SUM(summa) as `summa_profit` FROM `profit` WHERE login = '$login' AND status = '1'");
        if ($result_summa && $row = $result_summa->fetch_assoc()) {
            $summa_profit = $row['summa_profit'];
            $result_summa->free();
        }
        
        return $summa_profit;
    }
    
    //сумму участнику с каждого его реферала
    function summa_refer($login_r){
        global $mysqli;
        
        $summa = 0;
        $result_ref = $mysqli->query ("SELECT summa FROM `invest` WHERE login = '$login_r'");
        if ($result_ref) {
            while($row = $result_ref->fetch_assoc()) {
                $summa += $row['summa'];
            }
            $result_ref->free();
        }
        return $summa;
    }
    
    //получить email логина
    function login_email($login){
        global $mysqli;
        
        $email = '';
        
        $result = $mysqli->query("SELECT email FROM `users` WHERE login = '$login'");
        if($result && $row = $result->fetch_assoc()) {
            $email = $row['email'];
        }
        return $email;
    }
    
    //Имя участника
    function name_login($login) {
        global $mysqli;

        $name = '';
        $result = $mysqli->query("SELECT name FROM `users` WHERE login = '$login'");
        if($result && $row = $result->fetch_assoc()) {
            $name = $row['name'];
        }
        return $name;
    }
    
    //Фамилия участника
    function lastname_login($login) {
        global $mysqli;

        $lastname = '';
        $result = $mysqli->query("SELECT lastname FROM `users` WHERE login = '$login'");
        if($result && $row = $result->fetch_assoc()) {
            $lastname = $row['lastname'];
        }
        return $lastname;
    }
    
    //номер телефона участника
    function phone_login($login) {
        global $mysqli;

        $phone = '';
        $result = $mysqli->query("SELECT phone FROM `users` WHERE login = '$login'");
        if($result && $row = $result->fetch_assoc()) {
            $phone = $row['phone'];
        }
        return $phone;
    }
    
    //номер payeer участника
    function login_payeer($login) {
        global $mysqli;

        $payeer = '';
        $result = $mysqli->query("SELECT payeer FROM `users` WHERE login = '$login'");
        if($result && $row = $result->fetch_assoc()) {
            $payeer = $row['payeer'];
        }
        return $payeer;
    }
    
    //номер perfect участника
    function login_perfect($login) {
        global $mysqli;

        $perfect = '';
        $result = $mysqli->query("SELECT perfect FROM `users` WHERE login = '$login'");
        if($result && $row = $result->fetch_assoc()) {
            $perfect = $row['perfect'];
        }
        return $perfect;
    }
    
    //проверка логина на занятость
    function check_login($login) {
        global $mysqli;

        $result = $mysqli->query("SELECT login FROM users WHERE login = '$login'");
        if($result && $result->num_rows > 0){
            return true;
        } else {
            return false;
        }
    }
    
    //добавление вклада
    function add_transfer($login, $transfer) {
        global $mysqli;
    
        $refer = referovod_login($login);//получаем реферала
        $time = time();
        $result = $mysqli->query ("INSERT INTO `invest` (`login`, `transfer`, `date`, `refer`) VALUES ('$login', '$transfer', '$time', '$refer')");

        return (bool)$result;
    }
    
    /*добавить депозита*/
    function add_depozit($login, $summa, $time_depozit, $percent) {
        global $mysqli;

        $date_start = $time = time();//Время вклада
        $date_end = $date_start + $time_depozit;//время окончания работы депозита
        $summa_plus = $summa + ($summa / 100 * $percent);//сумма +  проценты
        $add_depozit = $mysqli->query ("INSERT INTO `depozit` (login, summa, summa_plus, date_start, date_end) VALUES ('$login', '$summa', '$summa_plus', '$date_start', '$date_end')");
        
        return (bool)$add_depozit;
    }
    
    /*добавить выплату*/
    function add_payment($login, $summa) {
        global $mysqli;
        $time = time();
        $add_payment = $mysqli->query("INSERT INTO `payment` (login, summa, date) VALUES ('$login', '$summa' '$time')");
        if($add_payment){
            return true;
        }
        else{
            return false;
        }
    }
    
    //последнее посещение
    function last_visit($login){
        global $mysqli;
        $visit = '';
        $result = $mysqli->query("SELECT date FROM `visit` WHERE login = '$login' ORDER BY id DESC LIMIT 1");
        if ($result && $row = $result->fetch_assoc()) {
            $visit = $row['date'];
            $result->free();
        }
        return $visit;
    }
    
    //последний зарегистровавшийся участиник
    function last_user(){
        global $mysqli;
        $login = '';
        
        $result = $mysqli->query("SELECT login FROM `users` ORDER BY id DESC LIMIT 1");
        if ($result && $row = $result->fetch_assoc()) {
            $login = $row['login'];
            $result->free();
        }
        
        return $login;
    }
    
    //последний вклад в систему
    function last_invest(){
        global $mysqli;
        $summa = '';
        $login = '';
        $result = $mysqli->query("SELECT summa, login FROM `invest` WHERE status = '1' ORDER BY id DESC LIMIT 1");
        if($result->num_rows <= 0){
            return "Нету :(";
        }
        if ($result && $row = $result->fetch_assoc()) {
            $summa = $row['summa'];
            $login = $row['login'];
            $result->free();
        }
        if($summa === ""){
            $summa = 0;
        }
        
        return $summa." руб - ".$login;
    }
    
    
    //последний выввод из системы
    function last_payment(){
        global $mysqli;
        $summa = '';
        $login = '';
        $result = $mysqli->query("SELECT summa, login FROM `payment` WHERE status = '1' ORDER BY id DESC LIMIT 1");
        if($result->num_rows <= 0){
            return "Нету :(";
        }
        if ($result && $row = $result->fetch_assoc()) {
            $summa = $row['summa'];
            $login = $row['login'];
            $result->free();
        }
        if($summa === ""){
            $summa = 0;
        }
        
        return $summa." руб - ".$login;
    }
    
    //считаем колличество рефералов
    function count_referals($login){
        global $mysqli;
        $count = 0;
        $result = $mysqli->query("SELECT count(*) as `count` FROM `users` WHERE refer = '$login'");
        if ($result && $row = $result->fetch_assoc()) {
            $count = $row['count'];
            $result->free();
        }
        return $count;
    }

    //топ инвесторов, топ вкладов
    function get_top_depozits() {
        global $mysqli;
        $list = [];
        $sql = "SELECT login, date_start, summa FROM depozit ORDER BY summa DESC LIMIT 0, 10;";
        $res = $mysqli->query($sql);
        if ($res) {
            while ($row = $res->fetch_assoc()) {    
                $list[] = [
                    "login"=>$row["login"], 
                    "date"=>$row["date_start"],
                    "summa"=> $row["summa"]
                ];  
            }
        }
        return $list;
    }


    //топ инвесторов, топ вкладов
    function get_top_payments() {
        global $mysqli;
        $list = [];
        $sql = "SELECT login, date, summa FROM payment ORDER BY summa DESC LIMIT 0, 10;";
        $res = $mysqli->query($sql);
        if ($res) {
            while ($row = $res->fetch_assoc()) {    
                $list[] = [
                    "login"=>$row["login"], 
                    "date"=>$row["date"],
                    "summa"=> $row["summa"]
                ];
            }
        }
        return $list;
    }


        //топ рефераводов
    function get_top_refer($count=10) {
        $count = (int)$count;
        global $mysqli;
        $list = [];
        $sql = "SELECT distinct u.refer as login, (SELECT count(*) FROM users WHERE refer = u.refer) as count, (SELECT count(distinct login) FROM invest WHERE refer = u.refer AND status = 1) as active_count FROM users u  ORDER BY count desc, active_count DESC LIMIT 0, {$count};";
        $result = $mysqli->query($sql);
        if ($result) {
            while ($row = $result->fetch_assoc()) {
                $list[] = $row;
            }
        }
        return $list;
    }
    
    //кол-во новостей
    function count_news(){
        global $mysqli;
        $count = 0;
        $result = $mysqli->query("SELECT count(*) as `count` FROM `news`");
        if ($result && $row = $result->fetch_assoc()) {
            $count = $row['count'];
            $result->free();
        }
        return $count;
    }
    
    function get_m(){
        global $mysqli;
        $num = 0;
        $num = date("m",time());
        $num = (int)$num;
        $num--;
        return $num;
    }
    
    //количество активных рефералов
    function count_acitve_refer($login){
        global $mysqli;
        
        $sql = "SELECT count(DISTINCT login) as count FROM `invest` WHERE refer = '$login' AND status = 1";
        $count_acitve_refer = 0;
        $result = $mysqli->query($sql);
        if ($result) {
            if ($row = $result->fetch_assoc()) {
                $count_acitve_refer = intval($row['count']);
            }
            $result->free();
        }
        
        return $count_acitve_refer;
    }
    
    //начисления
    function add_depozit_plans($login, $summa, $day_invest, $percent_day){
        global $mysqli;
        
        $summa_plus = 0;
        $date_start = time();
        
        $percent = $percent_day;
        $count = $day_invest;
        $seconds = 86400;
        
        $date_end = $date_start + $seconds * $count;
        $add_depozit = $mysqli->query ("INSERT INTO `depozit` (login, summa, summa_plus, date_start, date_end) VALUES ('$login', '$summa', '$summa_plus', '$date_start', '$date_end')");
        
        #получаем id депозита
        $result_id = $mysqli->query("SELECT id FROM `depozit` WHERE login = '$login' AND summa = '$summa' AND date_start = '$date_start' AND date_end = '$date_end'");
        if ($result_id && $row = $result_id->fetch_assoc()) {
            $depozit_id = $row['id'];
        }
        
        #добавление начислений!
        $summa = $summa / 100 * $percent;
        $date_end = time();
        for ($i = 1; $i <= $count; $i++){
            $date_end += $seconds;
            $result_profit = $mysqli->query ("INSERT INTO `profit` (login, depozit_id, date_end, summa) VALUES ('$login', '$depozit_id', '$date_end', '$summa')");
        }
        if($result_profit && $add_depozit){
            $message = "Депозит добавлен успешно!";
        } else {
            $message = "";
        }
        
        return $message;
    }
    
    //всего начислено профитов
    function count_dep($id){
        global $mysqli;
        $count = 0;
        $result = $mysqli->query("SELECT * FROM `profit` WHERE depozit_id = '$id' AND status = '1'");
        while ($result && $row = $result->fetch_assoc()) {
            $count++;
        }
        $result->free();
        return $count;
    }
    
    //сумма начислнных прфитов
    function summa_dep($id){
        global $mysqli;
        $summa = 0;
        $result = $mysqli->query("SELECT summa FROM `profit` WHERE depozit_id = '$id' AND status = '1'");
        while ($result && $row = $result->fetch_assoc()) {
            $summa += $row['summa'];
        }
        $result->free();
        return $summa;
    }
    
    //всего профитов
    function count_dep_all($id){
        global $mysqli;
        $count = 0;
        $result = $mysqli->query("SELECT * FROM `profit` WHERE depozit_id = '$id'");
        while ($result && $row = $result->fetch_assoc()) {
            $count++;
        }
        $result->free();
        return $count;
    }
?>