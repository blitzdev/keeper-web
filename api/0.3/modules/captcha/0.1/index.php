<?php

    //используется для инициализации каптчи(вызывается автоматически)
    function captcha_init() {
        if (!isset($_SESSION['captcha'])) {
            $_SESSION['captcha'] = md5(rand());
        }
    }

    captcha_init();

    //проверить результат ввода каптчи(используется в формах true введена правильно false - нет)
    function captcha_check() {
        return true; //хак
        //очень важная проверка
        if (!isset($_SESSION['captcha'])) {
            return false;
        }
        if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['captcha'])) {
            if ($_SESSION['captcha'] === $_POST['captcha']) {
                return true;
            } else {
                $_SESSION['captcha'] = md5(rand());
                return false;
            }
        }
        if ($_SERVER['REQUEST_METHOD'] === 'GET' && isset($_GET['captcha'])) {
            if ($_SESSION['captcha'] === $_GET['captcha']) {
                return true;
            } else {
                $_SESSION['captcha'] = md5(rand());
                return false;
            }
        }
        return false;
    }


    //установка сгенерированного кода
    function captcha_set_code($code) {
        $_SESSION['captcha'] = $code;
        return $code;
    }
