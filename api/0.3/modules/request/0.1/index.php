<?php
    //проверить по какому протоколу запрошен сайт
    function request_is_https() {
        if (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] === "on") {
            return true;
        }
        if (isset($_SERVER["SERVER_PORT"]) && $_SERVER["SERVER_PORT"] == 443) {
            return true;
        }
        return false;
    }

    //получить протокол сайта
    function request_get_protocol() {
        if (request_is_https()) {
            return "https://";
        }
        return "http://";
    }
