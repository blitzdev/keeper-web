<?php
    if (!function_exists("format_money")) {
        //форматируем обычные валюты
        function format_money($money) {
            return sprintf("%01.2f", $money);
        }
    }

    if (!function_exists("format_number")) {
        //форматируем обычные валюты
        function format_number($number) {
            return sprintf("%01.2f", $number);
        }
    }


    if (!function_exists("format_btc")) {
        //форматируем биткойны
        function format_btc($money) {
            return sprintf("%01.8f", $money);
        }
    }

    if (!function_exists("format_date")) {
        //форматируем дату(длинный формат)
        function format_date($date) {
            return date('Y-m-d H:i:s', (int)$date);
        }
    }


    if (!function_exists("format_date_short")) {
        //форматируем дату(короткий формат)
        function format_date_short($date) {
            return date('Y-m-d', (int)$date);
        }
    }
