<?php

    //получение баланса
    function perfectmoney__get_balance($wallet, $api_id, $api_key) {
        // trying to open URL to process PerfectMoney Spend request
        $f = fopen(
            //'https://perfectmoney.is/acct/balance.asp?AccountID='.urlencode($api_id).'&PassPhrase='.urlencode($api_key)
            'https://perfectmoney.is/acct/balance.asp?'.http_build_query([
                'AccountID' => $api_id,
                'PassPhrase' => $api_key,
                ])
            , 'rb');
        if ($f === false) {
           return message_error('error openning url');
        }

        // getting data
        $out = "";
        while (!feof($f)) {
            $out .= fgets($f);
        }
        fclose($f);

        // searching for hidden fields
        if (!preg_match_all("/<input name='(.*)' type='hidden' value='(.*)'>/", $out, $result, PREG_SET_ORDER)) {
           return message_error('Ivalid output');
        }

        // putting data to array
        foreach ($result as $item){
           $key = $item[1];
           $ar[$key] = $item[2];
        }

        if (isset($ar["ERROR"])) {
            return message_error($ar["ERROR"]);
        }

        foreach ($ar as $key=>$val) {
            if (substr($key, 0, 1) === "U") {
                if (!isset($list["usd"])) {
                    $list["usd"] = 0;
                }
                $list["usd"] += (float)$val;
            } else if (substr($key, 0, 1) === "E") {
                if (!isset($list["eur"])) {
                    $list["eur"] = 0;
                }
                $list["eur"] += (float)$val;
            } else if (substr($key, 0, 1) === "R") {
                if (!isset($list["rub"])) {
                    $list["rub"] = 0;
                }
                $list["rub"] += (float)$val;
            }
        }
        return message_success("", [
            "list" => $list,
        ]);
    }


    //проверка авторизации
    function perfectmoney__is_valid($wallet, $api_id, $api_key) {
        $res = perfectmoney__get_balance($wallet, $api_id, $api_key);
        if (!$res["error"]) {
            $res = message_success("Вы успешно авторизовались!");
        }
        return $res;
    }


    //получить историю
    function perfectmoney__get_history($wallet, $api_id, $api_key, $date_start=false, $date_end=false) {
        if (!$date_start) {
            $date_start = strtotime("-7 days");
        }
        if (!$date_end) {
            $date_end = time();
        }

        if ($date_end - $date_start >= 15 * 86400) {
            $date_start = $date_end - 14 * 86400;
        }

        $start_day = date("d", $date_start);
        $start_month = date("m", $date_start);
        $start_year = date("Y", $date_start);

        $end_day = date("d", $date_end);
        $end_month = date("m", $date_end);
        $end_year = date("Y", $date_end);

        $url = "https://perfectmoney.is/acct/historycsv.asp?";

        $params = [
            "startday" => (int)$start_day,
            "startmonth" => (int)$start_month,
            "startyear" => (int)$start_year,

            "endday" => (int)$end_day,
            "endmonth" => (int)$end_month,
            "endyear" => (int)$end_year,

            "AccountID" => $api_id,
            "PassPhrase" => $api_key,
        ];

        $url = $url.http_build_query($params);
        $f = fopen($url, "rb");
        if ($f === false) {
           return message_error('error openning url');
        }

        //getting data to array (line per item)
        $lines = array();
        while (!feof($f)) {
            $lines[] = trim(fgets($f));
        }
        fclose($f);
        // try parsing data to array
        if ($lines[0] != 'Time,Type,Batch,Currency,Amount,Fee,Payer Account,Payee Account,Payment ID,Memo') {
           // print error message
           return message_error($lines[0]);
        }
        // do parsing
        $list = array();
        $n = count($lines);
        for ($i=1; $i < $n; $i++) {
            $item = explode(",", $lines[$i], 10);
            if (count($item)!=10) {
              continue; // line is invalid - pass to next one
            }

            $date = strtotime($item[0]);
            $item_named["transaction"] = $item[2];
            $item_named["date"] = date("d.m.Y", $date);
            $item_named['time'] = date("H:i:s", $date);
            $item_named["datetime"] = $item_named["date"]." ".$item_named['time'];
            $item_named['sum'] = (float)$item[4] < 0 ? -(float)$item[4] : (float)$item[4];
            $item_named['currency'] = strtolower($item[3]);
            $item_named['comment'] = (string)$item[9];
            $item_named['type'] = $item[1] === "Income" ? "income" : "expenditure";
            $item_named["status"] = "success";
            $item_named["account"] = $item[6];
            $item_named['provider'] = "PerfectMoney";
            $item_named['commission_sum'] = $item[5];
            $item_named['commission_currency'] = strtolower($item[3]);

            array_unshift($list, $item_named);
        }

        return message_success("", [
            "list" => $list,
        ]);
    }


    //отправить деньги
    function perfectmoney__payment($wallet, $api_id, $api_key, $number, $amount, $currency="usd", $comment="") {
        if (substr($number, 0, 1) !== substr($wallet, 0, 1)) {
            return message_error("Перевод возможен только между кошельками одинакового типа!");
        }
        if (substr($number, 0, 1) == "U" && $currency !== "usd") {
            return message_error("Кошелек не поддерижвает эту валюту!");
        }
        if (substr($number, 0, 1) == "E" && $currency !== "eur") {
            return message_error("Кошелек не поддерижвает эту валюту!");
        }
        if (substr($number, 0, 1) == "B" && $currency !== "btc") {
            return message_error("Кошелек не поддерижвает эту валюту!");
        }
        $params = [
            "AccountID" => $api_id,
            "PassPhrase" => $api_key,
            "Payer_Account" => $wallet,
            "Payee_Account" => $number,
            "Amount" => number_format($amount, 2, '.', ''),
            "PAY_IN" => 1,
            "PAYMENT_ID" => time(),
            "Memo" => $comment,
        ];

        $url = "https://perfectmoney.is/acct/confirm.asp?";

        $f = fopen($url.http_build_query($params), 'rb');

        if($f === false) {
            return message_error("Ошибка выплаты!");
        }

        $out = "";
        while(!feof($f)) {
            $out .= fgets($f);
        }

        fclose($f);

        if(!preg_match_all("/<input name='(.*)' type='hidden' value='(.*)'>/", $out, $data, PREG_SET_ORDER)) {
            return message_error("Ошибка выплаты!");
        }

        $result = [];
        foreach($data as $item){
           $result[$item[1]] = $item[2];
        }

        if (isset($result["ERROR"])) {
            return message_error($result["ERROR"]);
        }

        return message_success("Выплата осуществлена!", [
            "id" => $result['PAYMENT_ID'],
            "sms" => false,
        ]);
    }
