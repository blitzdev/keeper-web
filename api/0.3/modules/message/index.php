<?php

    function message_success($message="", $extra_data=[]) {
        $data = [];
        $data['error'] = false;
        $data['message'] = $message;
        $data['data'] = $extra_data;
        return $data;
    }


    function message_error($message="", $extra_data=[], $code=true) {
        $data = [];
        $data['error'] = $code;
        $data['message'] = $message;
        $data['data'] = $extra_data;
        return $data;
    }
