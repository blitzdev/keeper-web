<?

    include_once(__DIR__."/error.php");
    require_once(__DIR__."/conf.php");


    require_once(ROOT."/wallets/payeer.php");
    
    require_once(ROOT."/libs/qiwi/QiwiApi.php");
    
    use Qiwi\QiwiApi;


    header('Content-Type: application/json; charset=utf-8');
    session_start();

    $ip = $_SERVER['REMOTE_ADDR'];
    $license_key =  request('license_key');
    $function = request('function');
    $hardware = request('hardware');
    $currency = strtolower(request('currency'));




	
	if ($function === 'get_free_license') {
		$sql = "INSERT INTO users SET ";
		$sql .= "license_key = ".escape_db(md5(time()."sdfdsfsdfdsfsdfds"));
		$sql .= ", expiration_license_key = ".escape_db(time() + 6 * 30 * 86400);
		$sql .= ", status = ".escape_db(1);

		$mysqli->query($sql);
		$user = get_row_by_id("users", $mysqli->insert_id);

		//echo "Новая лицензия: <b>".$invest["license_key"]."</b> до ".format_date($invest["expiration_license_key"]);
		//echo "<br><b>Blitz-Market</b>";
		
		$result = [
            "license_key" => $user["license_key"], //ключ лицензии
            "date_expiration" => format_date($user["expiration_license_key"]), //дата до которой лицензия актуальна
            "status" => (bool)$user["status"], //активная
        ];
        $data = [
        	"error" => false,
        	"message" => "",
        	"data" => $result,
        ];
        send_json($data);
	}

    
    function write_log_error($res, $phone="") {
        ob_start();
        print_r($res);
        $txt = ob_get_clean();
        file_put_contents(ROOT."/tmp/{$phone}_log.txt", date("d-m-Y H:i:s", time())."\n".$txt."\n", FILE_APPEND);
    }


    $list_currency = ["rub", "usd", "eur",]; //список валют

    $hardware_key_web = "aa2282bb6da42e7d1d581b17acf847183f8badbe";


//throw_json_error("Технические работы!");



	$res = [
		"ip" => $ip,
		"license_key" => $license_key,
		"function" => $function,
		"post" => $_POST,
		"get" => $_GET,
		"server" => $_SERVER,
	];
	write_log_error($res);

        #throw_json_error("Серж, ебаный питушок! Смогу подключиться к дев версии api Keeper!", [
        #    "logout" => true,
        #]);
    if (!$ip || !$license_key || !$function) {
        throw_json_error("Вы передали не все параметры для доступа к сервису!");
    }
    log_write([$_GET, $_POST]);

    //проверка возможности доступа
    //ищим лицензию такую
    $user = get_row_by_field("users", "license_key", $license_key);
    if ($user["license_key"] !== $license_key) {
        throw_json_error("Неправильный ключ лицензии!", [
            "logout" => true,
        ]);
    }


    include_once(__DIR__."/logout_error.php");

    if ($user["status"] !== "1") {
        throw_json_error("Лицензия не активна!", [
            "logout" => true,
        ]);
    }
    //проверка актуальности лицензии
    if ($user["expiration_license_key"] - time() <= 0) {
        throw_json_error("Лицензия действительна до ".date("d-m-Y H:i:s", $user["expiration_license_key"])."!", [
            "logout" => true,
        ]);
    }

    if ($user["id"] == "1" || $user["id"] == "4") {
        //throw_json_error("Животина не достойна использовать Кипер!");
    }

    //проверка привязки
    if (!trim($user["hardware"])) {
        $sql = "UPDATE users SET hardware = ".escape_db($hardware)." WHERE id = ".escape_db($user['id']);
        if (!$mysqli->query($sql)) {
            throw_json_error("Ошибка инициализации привзяки к железу!");
        }
        $user = get_row_by_field("users", "license_key", $license_key);
    }
    $hardwares = [];
    $hardwares[] = $user["hardware"];
    $hardwares[] = $hardware_key_web;
    if (!in_array($hardware, $hardwares)) {
        //if ($user["hardware"] != $hardware) {
            /*throw_json_error("Запрещен запуск на этом устройстве! Используйте ключ на устройстве где он был активирован!", [
                "logout" => true,
            ]);*/
        //}
    }


    /*ВНИМАНИЕ НЕ ЗАБЫВАЙ ФИЛЬТРОВАТЬ ПО ПОЛЬЗОВАТЕЛЮ */

    /*ВНИМАНИЕ НЕ ЗАБЫВАЙ ФИЛЬТРОВАТЬ ПО ПОЛЬЗОВАТЕЛЮ */

    /*ВНИМАНИЕ НЕ ЗАБЫВАЙ ФИЛЬТРОВАТЬ ПО ПОЛЬЗОВАТЕЛЮ */

    /*ВНИМАНИЕ НЕ ЗАБЫВАЙ ФИЛЬТРОВАТЬ ПО ПОЛЬЗОВАТЕЛЮ */

    /*ВНИМАНИЕ НЕ ЗАБЫВАЙ ФИЛЬТРОВАТЬ ПО ПОЛЬЗОВАТЕЛЮ */

    /*ВНИМАНИЕ НЕ ЗАБЫВАЙ ФИЛЬТРОВАТЬ ПО ПОЛЬЗОВАТЕЛЮ */

    if ($function === 'wallet_get_list') {
        $where = "";
        /*if (isset($_REQUEST["status"])) {
            if (request("status")) {
                $where .= " AND status = ".escape_db(1);
            } else {
                $where .= " AND status = ".escape_db(0);
            }
        }*/
        $where .= " AND status = ".escape_db(1);
        //$list = get_list("wallets", "status = 1 AND user_id = ".escape_db($user["id"]).$where);
        $list = extra_db_get_list(
            "wallets",
            "user_id = ".escape_db($user["id"]).$where,
            false,
            "id, wallet, system, status, comment");
        $result = [];
        $result["list"] = $list;
        $data = [
            "error" => false,
            "message" => "",
            "data" => $result,
        ];
        send_json($data);
    }


    //возвращает последнюю версию
    if ($function === 'check_update') {
        $result = [
            "version" => 20160716, //версия
            "link" => "https://yadi.sk/d/TIYihNZtswimM",
        ];
        $data = [
            "error" => false,
            "message" => "",
            "data" => $result,
        ];
        send_json($data);
    }


    if ($function === 'wallet_get_balances') {
        $where = "";
        $wallet_id = (int)request("wallet_id");
        $wallet = get_row_by_id("wallets", $wallet_id);
        if (!$wallet) {
            throw_json_error("Вы передали некорректный идентификатор кошелька!");
        }
        /* ОЧЕНь ВАЖНО */
        if ((int)$user["id"] !== (int)$wallet["user_id"]) {
            throw_json_error("Кошелек не принадлежит Вам!");
        }
        if (!(bool)$wallet["status"]) {
            throw_json_error("Кошелек не активирован в системе!");
        }
        //throw_json_error("test");
        $result = [];
        if ($wallet["system"] === "qiwi") {
            //$_wallet = new BM_FullQiwi($wallet["api_id"], $wallet["api_key"]);
            $_wallet = new QiwiApi($wallet["api_id"], $wallet["api_key"]);
            $result = $_wallet->getBalanceList();
        } else if ($wallet["system"] === "payeer"){
            $result = payeer_get_balance($wallet["wallet"], $wallet["api_id"], $wallet["api_key"]);
        } else if ($wallet["system"] === "advcash"){
            $result = advcash__get_balance($wallet["wallet"], $wallet["api_id"], $wallet["api_key"]);
        } else if ($wallet["system"] === "perfectmoney"){
            $result = perfectmoney__get_balance($wallet["wallet"], $wallet["api_id"], $wallet["api_key"]);
        }
        send_json($result);
    }



    if ($function === 'update_balances') {
        $wallet_list = get_list(
            "wallets",
            "status = 1 AND user_id = ".escape_db($user["id"]));
        $data = [];
        foreach ($wallet_list as $wallet) {

            if ($wallet["system"] === "qiwi") {
                //$_wallet = new BM_FullQiwi($wallet["api_id"], $wallet["api_key"]);
                //$list = $_wallet->balances();
                $_wallet = new QiwiApi($wallet["api_id"], $wallet["api_key"]);
                $list = $_wallet->getBalanceList();
            } else if ($wallet["system"] === "payeer"){
                $list = payeer_get_balance($wallet["wallet"], $wallet["api_id"], $wallet["api_key"]);
            } else if ($wallet["system"] === "advcash"){
                $list = advcash__get_balance($wallet["wallet"], $wallet["api_id"], $wallet["api_key"]);
            } else if ($wallet["system"] === "perfectmoney"){
                $list = perfectmoney__get_balance($wallet["wallet"], $wallet["api_id"], $wallet["api_key"]);
            } else {
                $list["data"]["list"] = [];
            }
            $data[] = [
                "id" => (int)$wallet["id"],
                "list" => $list["data"]["list"],
            ];
        }
        send_json(message_success(
            "Баланс кошельков", [
                "balances" => $data,
            ]
        ));
    }


    if ($function === 'wallet_get_history') {
        $start = get("start") ?: date("Y-m-d", strtotime("-30 days"));
        $finish = get("finish") ?: date("Y-m-d", time() + 86400);

        if ($start) {
            $start = date("Y-m-d", strtotime($start));
        }

        if ($finish) {
            $finish = date("Y-m-d", strtotime($finish));
        }
        $where = "";
        $wallet_id = (int)request("wallet_id");
        $wallet = get_row_by_id("wallets", $wallet_id);
        if (!$wallet) {
            throw_json_error("Вы передали некорректный идентификатор кошелька!");
        }
        /* ОЧЕНь ВАЖНО */
        if ((int)$user["id"] !== (int)$wallet["user_id"]) {
            throw_json_error("Кошелек не принадлежит Вам!");
        }
        if (!(bool)$wallet["status"]) {
            throw_json_error("Кошелек не активирован в системе!");
        }

        if (!in_array($wallet["system"], ["qiwi", "advcash", "perfectmoney", "payeer", ])) {
            throw_json_error("Для этой платежной системы функция недоступна!");
        }

        $result = [];
        $cache_file = __DIR__."/tmp/cache__".$wallet["api_id"]."_{$start}_{$finish}_wallet_get_history.txt";
        if (true/*!file_exists($cache_file) || time() - filemtime($cache_file) > CACHE_HISTORY*/) {
            if ($wallet["system"] === "qiwi") {
                //$_wallet = new BM_FullQiwi($wallet["api_id"], $wallet["api_key"]);
                //$result = $_wallet->history($start, $finish);
                $_wallet = new QiwiApi($wallet["api_id"], $wallet["api_key"]);
                $result = $_wallet->getHistoryList();
            } else if ($wallet["system"] === "payeer") {
                $start = strtotime($start);
                $finish = strtotime($finish);
                $result = payeer_get_history($wallet["wallet"], $wallet["api_id"], $wallet["api_key"], $start, $finish);
            } else if ($wallet["system"] === "advcash") {
                $start = strtotime($start);
                $finish = strtotime($finish);
                $result = advcash__get_history($wallet["wallet"], $wallet["api_id"], $wallet["api_key"], $start, $finish);
            } else if ($wallet["system"] === "perfectmoney") {
                $start = strtotime($start);
                $finish = strtotime($finish);
                $result = perfectmoney__get_history($wallet["wallet"], $wallet["api_id"], $wallet["api_key"], $start, $finish);
            }

            file_put_contents($cache_file, json_encode($result));
        } else {
            $result = json_decode(file_get_contents($cache_file), true);
        }
        send_json($result);
    }


    //добавить кошелек в систему
    if ($function === 'wallet_add') {
        if (empty(request('wallet'))) {
            throw_json_error("Пустой номер кошелька!");
        }
        $wallet = trim(request('wallet'));

        if (!in_array(request('system'), ["qiwi", "payeer", "advcash", "perfectmoney", ])) {
            throw_json_error("Неподдерживаемая система платежей!");
        }
        $system = request('system');
        $comment = request('comment');
        if (empty(request('api_id'))) {
            throw_json_error("Пустой API ID!");
        }
        $api_id = request('api_id');
        if (empty(request('api_key'))) {
            throw_json_error("Пустой API KEY!");
        }
        $api_key = request('api_key');
        
        $sql = "SELECT COUNT(id) FROM wallets WHERE wallet = ".escape_db($wallet)." AND user_id = ".escape_db($user['user_id']);
        if (get_value($sql) > 0) {
            throw_json_error();
        }


        if ($system === "qiwi") {

            //$_wallet = new BM_FullQiwi($api_id, $api_key);
            //$res = $_wallet->balances();
            $_wallet = new QiwiApi($api_id, $api_key);
            $res = $_wallet->getBalanceList();
            
            //throw_json_error($wallet);
            if ($res["error"] || !$res["data"]["list"]) {
            	throw_json_error($res['message']);
            }
        } else if ($system === "payeer") {
            $res = payeer_is_valid($wallet, $api_id, $api_key);
            if ($res["error"]) {
                send_json($res);
            }
        } else if ($system === "advcash") {
            $res = advcash__is_valid($wallet, $api_id, $api_key);
            if ($res["error"]) {
                send_json($res);
            }
        } else if ($system === "perfectmoney") {
            $res = perfectmoney__is_valid($wallet, $api_id, $api_key);
            if ($res["error"]) {
                send_json($res);
            }
        }



        $sql = "INSERT INTO wallets SET ";
        $sql .= "user_id = ".escape_db($user['id']);
        $sql .= ", wallet = ".escape_db($wallet);
        $sql .= ", system = ".escape_db($system);
        $sql .= ", comment = ".escape_db($comment);
        $sql .= ", api_id = ".escape_db($api_id);
        $sql .= ", api_key = ".escape_db($api_key);
        $sql .= ", status = ".escape_db(1);
        log_write($sql);

        if (!$mysqli->query($sql)) {
            throw_json_error("Ошибка добавления кошелька!");
        }

        $result = [
            "wallet_id" => $mysqli->insert_id,
        ];

        $data = [
            "error" => false,
            "message" => "Кошелек успешно добавлен в систему!",
            "data" => $result,
        ];
        send_json($data);
    }



    if ($function === 'wallet_delete') {
        $where = "";
        $wallet_id = (int)request("wallet_id");
        $wallet = get_row_by_id("wallets", $wallet_id);
        if (!$wallet) {
            throw_json_error("Вы передали некорректный идентификатор кошелька!");
        }
        /* ОЧЕНь ВАЖНО */
        if ((int)$user["id"] !== (int)$wallet["user_id"]) {
            throw_json_error("Кошелек не принадлежит Вам!");
        }
        $sql = "DELETE FROM wallets WHERE id = ".escape_db($wallet['id']);
        if (!$mysqli->query($sql)) {
            throw_json_error("Ошибка удаления кошелька из системы!");
        }
        $data = [
            "error" => false,
            "message" => "Кошелек успешно удален!",
            "data" => [],
        ];
        send_json($data);
    }


    if ($function === 'check_license') {
        $result = [
            "license_key" => $user["license_key"], //ключ лицензии
            "date_expiration" => format_date($user["expiration_license_key"]), //дата до которой лицензия актуальна
            "status" => (bool)$user["status"], //активная
        ];
        $data = [
        	"error" => false,
        	"message" => "",
        	"data" => $result,
        ];
        send_json($data);
    }


    if ($function === 'wallet_sendmoney') {
        $where = "";
        $wallet_id = (int)request("wallet_id");
        $wallet = get_row_by_id("wallets", $wallet_id);
        if (!$wallet) {
            throw_json_error("Вы передали некорректный идентификатор кошелька!");
        }
        /* ОЧЕНь ВАЖНО */
        if ((int)$user["id"] !== (int)$wallet["user_id"]) {
            throw_json_error("Кошелек не принадлежит Вам!");
        }
        if (!request('wallet_to')) {
            throw_json_error("Вы неуказали кошелек для перевода!");
        }

        if (!in_array($currency, $list_currency)) {
            throw_json_error("Неподдерживаемая валюта!");
        }

        $wallet_to = request("wallet_to");
        /*
        TODO  платежная система, валюта, метод пееревода
        */
        $sum = request("sum");
if ($sum > 15) {
    //throw_json_error("Ограничение суммы на тесты до 15 рубелй!");
}



        $comment = (string)request('comment');

        if ($wallet["system"] === "qiwi") {
            if ($currency !== 'rub') {
                throw_json_error("Поддерживаются только RUB!");
            }
           //$_wallet = new BM_FullQiwi($wallet["api_id"], $wallet["api_key"]);
            //$data = $_wallet->pay($wallet_to, $sum, $comment);
            $_wallet = new QiwiApi($wallet["api_id"], $wallet["api_key"]);
            $data = $_wallet->transfer($wallet_to, $sum, $currency, $comment);
        } else if ($wallet["system"] === "payeer") {
            $data = payeer_payment(
                $wallet["wallet"],
                $wallet["api_id"],
                $wallet["api_key"],
                $wallet_to,
                $sum,
                $currency,
                $comment
            );
        } else if ($wallet["system"] === "advcash") {
            $data = advcash__payment(
                $wallet["wallet"],
                $wallet["api_id"],
                $wallet["api_key"],
                $wallet_to,
                $sum,
                $currency,
                $comment
            );
        } else if ($wallet["system"] === "perfectmoney") {
            $data = perfectmoney__payment(
                $wallet["wallet"],
                $wallet["api_id"],
                $wallet["api_key"],
                $wallet_to,
                $sum,
                $currency,
                $comment
            );
        }

        //сбрасываем для обоих кошельков кэш истории
        $cache_file = __DIR__."/tmp/cache__".$wallet["api_id"]."_*_wallet_get_history.txt";
        array_map('unlink', glob($cache_file));

        $cache_file = __DIR__."/tmp/cache__".$wallet_to."_*_wallet_get_history.txt";
        array_map('unlink', glob($cache_file));

        send_json($data);
    }


	
	

    if ($function === 'wallet_qiwi_sms_confirm') {
        throw_json_error("Не поддерживается!");
        $where = "";
        $wallet_id = (int)request("wallet_id");
        $wallet = get_row_by_id("wallets", $wallet_id);
        if (!$wallet) {
            throw_json_error("Вы передали некорректный идентификатор кошелька!");
        }
        /* ОЧЕНь ВАЖНО */
        if ((int)$user["id"] !== (int)$wallet["user_id"]) {
            throw_json_error("Кошелек не принадлежит Вам!");
        }
        if (!request('sms')) {
            throw_json_error("Вы не указали SMS код!");
        }
        $sms = request("sms");

        $_wallet = new BM_FullQiwi($wallet["api_id"], $wallet["api_key"]);
        $data = $_wallet->sms($sms);

        send_json($data);
    }

    throw_json_error("Функция API отсутствует!");
