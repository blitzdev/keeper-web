<?php
    require_once __DIR__.'/../libs/Curl.php';
    require_once __DIR__.'/../libs/CDom.php';
    require_once __DIR__.'/../libs/simple_html_dom.php';

    use \Curl\Curl;


    //функция логгирования
    function write_log_error($res, $phone="") {
        ob_start();
        print_r($res);
        $txt = ob_get_clean();
        file_put_contents(__DIR__."/tmp/{$phone}_log.txt", date("d-m-Y H:i:s", time())."\n".$txt."\n", FILE_APPEND);
    }


    //функция создания временной директории
    function make_tmp_dir() {
        if (!is_dir(__DIR__."/tmp/")) {
            mkdir(__DIR__."/tmp/");
        }
    }

    //блок управления авторизацией
    function auth_qiwi(&$curl, $phone, $password) {
        auth_qiwi_0_2_2($curl, $phone, $password);
    }


    //авторизация из 0.2.3
    function auth_qiwi_0_2_3(&$curl, $phone, $password) {
        $curl->setOpt(CURLOPT_SSL_VERIFYPEER, 0);

        $curl->setCookieFile(__DIR__."/tmp/{$phone}_cookie.txt");
        $curl->setCookieJar(__DIR__."/tmp/{$phone}_jar.txt");
        $curl->setHeader('Content-Type', 'application/json');
        $curl->setHeader('X-Requested-With', 'XMLHttpRequest');
        $curl->setHeader('User-Agent', 'Mozilla/5.0 (X11; Ubuntu; Linux i686; rv:40.0) Gecko/20100101 Firefox/40.0');

        $res = $curl->post('https://qiwi.com/person/state.action');



        $data = [
            'login' => $phone,
            'password' => $password,
        ];

        $res = $curl->post('https://auth.qiwi.com/cas/tgts', json_encode($data));
        write_log_error($res, $phone);
        $TICKET = $res->entity->ticket;
        $data = [
            "ticket" => $TICKET,
            "service" => "https://qiwi.com/j_spring_cas_security_check",
        ];
        $res = $curl->post('https://auth.qiwi.com/cas/sts', json_encode($data));
        write_log_error($res, $phone);
        $res = $curl->post('https://auth.qiwi.com/cas/sts', json_encode($data));
        write_log_error($res, $phone);

        $TICKET = $res->entity->ticket;
        $res = $curl->get('https://qiwi.com/j_spring_cas_security_check?ticket='.$TICKET);
        write_log_error($res, $phone);
        $data = [
            "v" => 1,
        ];

        $res = $curl->get("https://auth.qiwi.com/app/proxy", $data);
        write_log_error($res, $phone);
        $res = $curl->post('https://qiwi.com/person/state.action');
        write_log_error($res, $phone);
    }
    //авторизация из 0.2.2
    function auth_qiwi_0_2_2(&$curl, $phone, $password) {
        $curl->setOpt(CURLOPT_SSL_VERIFYPEER, 0);

        $curl->setCookieFile(__DIR__."/tmp/{$phone}_cookie.txt");
        $curl->setCookieJar(__DIR__."/tmp/{$phone}_jar.txt");
        $curl->setHeader('Content-Type', 'application/json');
        $curl->setHeader('X-Requested-With', 'XMLHttpRequest');
        $curl->setHeader('User-Agent', 'Mozilla/5.0 (X11; Ubuntu; Linux i686; rv:40.0) Gecko/20100101 Firefox/40.0');


        $data = [
            'login' => $phone,
            'password' => $password,
        ];

        $res = $curl->post('https://auth.qiwi.com/cas/tgts', json_encode($data));
        write_log_error($res, $phone);
        $TICKET = $res->entity->ticket;
        $data = [
            "ticket" => $TICKET,
            "service" => "https://qiwi.com/j_spring_cas_security_check",
        ];
        $res = $curl->post('https://auth.qiwi.com/cas/sts', json_encode($data));
        write_log_error($res, $phone);
        $data = [
            "ticket" => $TICKET,
            "service" => "https://qiwi.com/j_spring_cas_security_check",
        ];
        $res = $curl->post('https://auth.qiwi.com/cas/sts', json_encode($data));
        write_log_error($res, $phone);
        $TICKET = $res->entity->ticket;
        $data = [
            "ticket" => $TICKET,
        ];
        $res = $curl->get('https://qiwi.com/j_spring_cas_security_check?ticket='.$TICKET);
        write_log_error($res, $phone);
    }




    //прием платежа по коду транзакции
    function get_sum_transaction_by_qiwi($phone, $password, $code) {
        make_tmp_dir();

        file_put_contents(__DIR__."/tmp/{$phone}_log.txt", "New\n", FILE_APPEND);

        $curl = new Curl();
        auth_qiwi($curl, $phone, $password);
        //авторизация закончена переходим к выплатам
        $res = $curl->get('https://qiwi.com/report/list.action?type=3');
        write_log_error($res, $phone);

        //https://github.com/amal/CDom
        $dom = CDom::fromString($res);

        //выбираем все успешные транзакции
        foreach($dom->find('div.reportsLine.status_SUCCESS') as $element) {
                $summa  = trim($element->find('div.IncomeWithExpend.income')->find('div.cash')->outerHtml());

                $summa = preg_replace('/[^0-9\.]+/', '', str_replace( ',', '.', $summa)) - 0;
                $transaction = trim($element->find('div.transaction'));
                if ($summa == "") {
                    continue;
                }
                if (mb_strpos(trim($element->find('div.IncomeWithExpend.income')->find('div.cash')), 'руб.' ) === false) {
                    continue;
                }

                if ($transaction !== $code) {
                    continue;
                }
                return (float)$summa;
        }
        return false;
    }


    //прием платежа по комментарию
    function get_sum_of_comment_by_qiwi($phone, $password, $comment) {
        make_tmp_dir();

        file_put_contents(__DIR__."/tmp/{$phone}_log.txt", "New\n", FILE_APPEND);

        $curl = new Curl();
        auth_qiwi($curl, $phone, $password);
        //авторизация закончена переходим к выплатам
        $res = $curl->get('https://qiwi.com/report/list.action?type=3');
        write_log_error($res, $phone);

        //https://github.com/amal/CDom
        $dom = CDom::fromString($res);

        //выбираем все успешные транзакции
        foreach($dom->find('div.reportsLine.status_SUCCESS') as $element) {
                $summa  = trim($element->find('div.IncomeWithExpend.income')->find('div.cash')->outerHtml());

                $summa = preg_replace('/[^0-9\.]+/', '', str_replace( ',', '.', $summa)) - 0;
                $transaction = trim($element->find('div.comment'));
                if ($summa == "") {
                    continue;
                }
                if (mb_strpos(trim($element->find('div.IncomeWithExpend.income')->find('div.cash')), 'руб.' ) === false) {
                    continue;
                }

                if ($transaction !== $comment) {
                    continue;
                }
                return (float)$summa;
        }
        return false;
    }


    //выплаты на кошелек(если возвращает false то это может быть как ошибка работы с сервсом так и выплата по смс)
    function payment_qiwi($phone, $password, $to, $sum, $comment = "") {
        make_tmp_dir();
        $id = str_replace(".", "", microtime(true));

        file_put_contents(__DIR__."/tmp/{$phone}_log.txt", "New\n", FILE_APPEND);

        $curl = new Curl();

        auth_qiwi($curl, $phone, $password);

        //авторизация закончена переходим к выплатам


        $curl->setHeader('Referer', 'https://qiwi.com/payment/form.action?provider=99');
        //$curl->setHeader('Content-Length', '183');
        $curl->setHeader('Host', 'qiwi.com');
        $curl->setHeader('Accept', 'application/vnd.qiwi.v2+json');

        $curl->setCookie('TestForThirdPartyCookie', 'yes');
        $curl->setCookie('ref', 'newsite_b1');
        $curl->setCookie('sms-alert', 'none');

        $data = [
            'id' => "$id",
            'sum' => [
                'amount' => "$sum",
                'currency' => "643"
            ],
            'source' => 'account_643',
            'paymentMethod' => [
                'type' => 'Account',
                'accountId' => "643"
            ],
            'comment' => $comment,
            'fields' => [
                "account" => $to,
            ],
        ];


        $res = $curl->post("https://qiwi.com/user/sinap/api/terms/99/validations/proxy.action", json_encode($data));
        write_log_error($res, $phone);
        $curl->setHeader('Referer', 'https://qiwi.com/payment/form.action?provider=99&state=confirm');
        //$curl->setHeader('Content-Length', '206');

        $data['fields']['_meta_pay_partner'] = "";

        $res = $curl->post("https://qiwi.com/user/sinap/api/terms/99/payments/proxy.action", json_encode($data));
        write_log_error($res, $phone);

        if ($res->data->body->transaction->state->code === "Accepted") {
            return $id;
        } else {
            file_put_contents(
                __DIR__."/tmp/{$phone}_json.txt",
                json_encode([
                    "id" => $id,
                    "sms" => ($res->data->body->transaction->state->code === "AwaitingSMSConfirmation"),
                ])
            );
            return false;
        }
    }


    //отправка кода смс подтверждения транзакции
    function payment_qiwi_send_code($phone, $password, $code) {
        make_tmp_dir();

        file_put_contents(__DIR__."/tmp/{$phone}_log.txt", "New\n", FILE_APPEND);

        $curl = new Curl();
        auth_qiwi($curl, $phone, $password);
        //авторизация закончена переходим к выплатам

        $curl->setHeader('Referer', 'https://qiwi.com/payment/form.action?provider=99');
        //$curl->setHeader('Content-Length', '183');
        $curl->setHeader('Host', 'qiwi.com');
        $curl->setHeader('Accept', 'application/vnd.qiwi.v2+json');

        $curl->setCookie('TestForThirdPartyCookie', 'yes');
        $curl->setCookie('ref', 'newsite_b1');
        $curl->setCookie('sms-alert', 'none');

        $curl->setHeader('Referer', 'https://qiwi.com/payment/form.action?provider=99&state=pay');

        $res = file_get_contents(__DIR__."/tmp/{$phone}_json.txt");
        $res = json_decode($res, true);

        $id = $res['id'];

        if (!$res["sms"]) {
            $res = [
                "error" => true,
                "message" => "Другой род ошибки - не смс подтверждение!",
            ];
            write_log_error($res, $phone);
            return false;
        }

        $data = [
            "smsCode" => $code,
        ];
        $res = $curl->post("https://qiwi.com/user/sinap/payments/{$id}/confirm/proxy.action", json_encode($data));


        if ($res->data->body->transaction->state->code === "Accepted") {
            return $res->data->body->paymentId;
        } else {
            file_put_contents(
                __DIR__."/tmp/{$phone}_json.txt",
                json_encode([
                    "id" => $id,
                ])
            );
            return false;
        }
    }
