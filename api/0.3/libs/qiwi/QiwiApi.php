<?php

namespace Qiwi;


class QiwiApi {
    function __construct($qiwi, $token) {
        if (!in_array ('curl', get_loaded_extensions())) {
            die('Not Install Curl Extension!');
        }
        $this->wallet = str_replace("+", "", $qiwi);
        $this->token = $token;
        $this->errorMessage;
        $this->baseUrl = "https://edge.qiwi.com";
        $this->currencyList = $this->getCurrencyList();
    }

    private function request($url, $method="GET", $data=[]) {
        $this->curl = curl_init();
        curl_setopt($this->curl,
            CURLOPT_URL,
            $this->baseUrl.$url.($method === "GET" ? "?".http_build_query($data) : "")
        );
        if ($method === "POST") {
            //curl_setopt($this->curl, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($this->curl, CURLOPT_POST, $method === "POST");
            curl_setopt($this->curl, CURLOPT_POSTFIELDS, json_encode($data));
        }
        curl_setopt($this->curl, CURLOPT_HTTPHEADER, [
            "Accept: application/json",
            "Content-Type: application/json",
            "Authorization: Bearer {$this->token}",
            //"User-Agent: ***",
        ]);
        curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, true);
        $data = curl_exec($this->curl);
        $data = json_decode($data, true);
        $httpcode = curl_getinfo($this->curl, CURLINFO_HTTP_CODE);
        if ($httpcode>=200 && $httpcode < 300) {
            return $data;
        }
        $this->errorMessage = isset(
            $data['errorCode'], $data['userMessage']) ? $data['userMessage'] : $this->getDescriptionError($httpcode);
        curl_close($this->curl);
        return false;
    }

    //получить описание ошибки
    private function getDescriptionError($code) {
        $codeList = [
            "400" => "Ошибка синтаксиса запроса (неправильный формат данных)",
            "401" => "Неверный токен или истек срок действия токена.",
            "404" => "Не удалось найти указанную информацию",
            "425" => "Слишком много запросов",
        ];
        return $codeList[(string)$code];
    }


    //получить баланс кошелька
    public function getBalanceList() {
        $res = $this->request("/funding-sources/v1/accounts/current");
        if ($res === false) {
            return $this->_err($this->getError());
        }
        $data = [];
        foreach ($res['accounts'] as $item) {
            if (!$item['hasBalance']) {
                continue;
            }
            if (in_array($item['balance']['currency'], array_keys(
                $this->currencyList))) {
                $data[
                    strtolower($this->currencyList[(string)$item['balance']['currency']])
                ] = $item['balance']['amount'];
            }
        }
        return $this->_ok("Баланс успешно получен", [
            "list" => $data,
        ]);
    }

    //сделать перевод
    public function transfer($wallet, $sum, $currency="RUB", $comment="") {
        $data = [
            "id" => (string)str_replace(".", "", microtime(true)),
            "sum" => [
                "amount" => (float)$sum,
                "currency" => "643",//$this->getCurrencyCode($currency)
            ],
            "paymentMethod" => [
                "type" => "Account",
                "accountId" => "643", //$this->getCurrencyCode($currency),
            ],
            "comment" => $comment,
            "fields" => [
                "account" => $wallet,
            ],
        ];
        //var_dump($data);
        $res = $this->request(
            "/sinap/api/v2/terms/99/payments", "POST", $data);
        if ($res === false) {
            return $this->_err($this->getError());
        }
        return $this->_ok("Перевод успешно выполнен!", [
            "id" => $res["id"],
            "sms" => false,
        ]);
    }


    public function getHistoryList($start=false, $end=false) {
        $res = $this->request("/payment-history/v1/persons/{$this->wallet}/payments", "GET", [
            "rows" => 50,
        ]);
        if ($res === false) {
            return $this->_err($this->getError());
        }
        $data = [];
        foreach ($res["data"] as $elem) {
            $item = [];
            $item['transaction'] = $elem['trmTxnId'];
            $date = strtotime($elem['date']);
            $item['date'] = date("d.m.Y", $date);
            $item['time'] = date("H:i:s", $date);
            $item['datetime'] = $item['date']." ".$item['time'];
            $item['sum'] = $elem['sum']['amount'];
            $item['currency'] = $this->currencyList[(string)$elem['sum']['currency']];
            $item['comment'] = isset($elem['comment']) ? $elem['comment'] : "";
            $item['type'] = $elem['type'] === "OUT" ? "expenditure" : "income";
            $item['status'] = $elem['status'] === "WAITING" ? "processed" : strtolower($elem['status']);
            $item['account'] = $elem['account'];
            $item['provider'] = $elem['provider']['shortName'];
            $item['commission_sum'] = $elem['commission']['amount'];
            $item['commission_currency'] = $this->currencyList[(string)$elem['commission']['currency']];
            $data[] = $item;
        }
        return $this->_ok("История успешно извлечена", [
            "list" => $data,
        ]);
    }

    private function getCurrencyList() {
        return [
            "643" => "RUB",
            "840" => "USD",
            "978" => "EUR",
        ];
    }

    private function getCurrencyCode($code) {
        foreach ($this->currencyList as $key=>$value) {
            if ($code === $value) {
                return $key;
            }
        }
        return "643";
    }

    public function getError() {
        return $this->errorMessage;
    }

    private function _ok($mes, $data=[]) {
        return [
            "error" => false,
            "message" => $mes,
            "data" => $data,
        ];
    }

    private function _err($mes, $data=[]) {
        return [
            "error" => true,
            "message" => $mes,
            "data" => $data,
        ];
    }
}