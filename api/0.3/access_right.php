<?php

  //проверка на доступность методов api для данной платформы
  function api_check_access($domain, $ip) {
      global $mysqli;

      $res = get_row_by_field('platform', "domain", $domain);

      if ($res && empty($res['ip'])) {
          $sql = "UPDATE `platform` SET `ip` = ".escape_db($ip)."  WHERE `domain` = ".escape_db($domain)." LIMIT 1;";

          $res = $mysqli->query($sql);
          if (!$res) {
              throw_json_error("Ошибка добавления ip!");
          }
      }
      //если ip соответствует домену ищем ip домен в базе.... Если есть разрешаем доступ
      $sql = "SELECT COUNT(*) FROM `platform` WHERE users_delete = '0' AND pay_status = '1' AND `domain` =".escape_db($domain)."  AND `ip` = ".escape_db($ip)." LIMIT 1;";
      return (int)get_value($sql) === 1;
  }


  //пополения разрешены
  function api_check_access_invest($domain) {
      global $mysqli;

      $platform = get_row_by_field('platform', "domain", $domain);
      return $platform['invest_status'] == 1;
  }

  //пополения через коментарии разрешены
  function api_check_access_invest_by_comment($domain) {
      global $mysqli;

      $platform = get_row_by_field('platform', "domain", $domain);
      return $platform['invest_by_comment_status'] == 1;
  }

  //выплаты разрешены
  function api_check_access_payment($domain) {
      global $mysqli;

      $platform = get_row_by_field('platform', "domain", $domain);
      return $platform['payment_status'] == 1;
  }
