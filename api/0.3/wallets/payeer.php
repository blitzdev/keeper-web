<?php
    require_once(ROOT."/modules/cpayeer.php");


    function payeer_is_valid($wallet, $api_id, $api_key) {
        $payeer = new CPayeer($wallet, $api_id, $api_key);
        if (!$payeer->isAuth()) {
        	$res = message_error($payeer->getErrors());
            return $res;
        }
        $res = message_success("Вы успешно авторизованы");
        return $res;
    }

    function payeer_payment($wallet, $api_id, $api_key, $number, $amount, $currency="rub", $comment="") {
        $payeer = new CPayeer($wallet, $api_id, $api_key);
        if (!$payeer->isAuth()) {
        	$res = message_error($payeer->getErrors()[0]);
            return $res;
        }
        //Проверка кошелька на существование
        if(!($payeer->checkUser(array('user' => $number,)))) {
            $res = message_error("Кошелек для перевода не существует!");
            return $res;
        }
        //выплата

        $arTransfer = $payeer->transfer(array(
            'curIn' => strtoupper($currency),
            'sum' => (float)$amount,
            'curOut' => strtoupper($currency),
            'to' => $number,
            'comment' => $comment,
        ));


        //проверка истории
        if (empty($arTransfer['historyId'])){
            $res = message_error($arTransfer["errors"][0]);
            return $res;
        }

        return message_success("Выплата осуществлена!", [
            "id" => (string)$arTransfer['historyId'],
            "sms" => false,
        ]);
    }


    function payeer_get_balance($wallet, $api_id, $api_key) {
        $payeer = new CPayeer($wallet, $api_id, $api_key);
        if (!$payeer->isAuth()) {
        	$res = message_error($payeer->getErrors()[0]);
            return $res;
        }

        $balance = $payeer->getBalance();

        if ($balance["auth_error"]) {
            $res = message_error($balance["errors"][0]);
            return $res;
        }
        $list = [];
        foreach ($balance["balance"] as $key=>$item) {
            $list[strtolower($key)] = (float)$item["DOSTUPNO"];
        }
        return message_success("", [
            "list" => $list,
        ]);
    }



    //получить историю
    function payeer_get_history($wallet, $api_id, $api_key, $date_start=false, $date_end=false) {

        if (!$date_start) {
            $date_start = strtotime("-7 days");
        }
        if (!$date_end) {
            $date_end = time();
        }

        $date_start = $date_end - 1 * 86400;
        $payeer = new CPayeer($wallet, $api_id, $api_key);
        $list = $payeer->getHistory(
            date("Y-m-d H:i:s", $date_start),
            date("Y-m-d H:i:s", $date_end)
        );


        if (isset($list["errors"][0])) {
            return message_error($list["errors"][0]);
        }


        $res = [];
        foreach ($list["history"] as $elem) {
            $item = [];
            $currency = strtolower($elem["creditedCurrency"]);
            $date = $elem["date"];
            $date = strtotime($date);
            $item["transaction"] = (string)$elem["id"];
            $item["date"] = date("d.m.Y", $date);
            $item['time'] = date("H:i:s", $date);
            $item["datetime"] = $item["date"]." ".$item['time'];
            $item['sum'] = $elem["creditedAmount"];
            $item['currency'] = $currency;
            $item['comment'] = (string)$elem["comment"];
            $item['type'] = $elem["to"] === $wallet ? "income" : "expenditure";
            if ($elem["status"] === "success") {
                $item["status"] = "success";
            } else if ($elem["status"] === "pending" || $elem["status"] === "waiting" || $elem["status"] === "processing") {
                $item["status"] = "precessed";
            } else {
                $item["status"] = "error";
            }
            $item["account"] = $elem["from"];
            $item['provider'] = "Payeer";
            $item['commission_sum'] = $elem["payeerFee"] + $elem["gateFee"];
            $item['commission_currency'] = $currency;

            //$res[] = $item;
            array_unshift($res, $item);
        }
        return message_success("", [
            "list" => $res,
        ]);
    }

